/*
 * options.c
 *
 * lcxterm - Linux Console X-like Terminal
 *
 * Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any
 * warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "qcurses.h"
#include "common.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <ctype.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include "keyboard.h"
#include "main.h"
#include "scrollback.h"
#include "forms.h"
#include "options.h"

/*
 * One option from the config file.
 */
struct option_struct {
    Q_OPTION option;
    char * value;
    char * name;
    char * default_value;
    char * comment;
};

/* The full path to the options file. */
static char home_directory_options_filename[FILENAME_SIZE];

/**
 * The --config command line argument, stored in main.c.
 */
extern char * q_config_filename;

/**
 * The --dotlcxterm-dir command line argument, stored in main.c.
 */
extern char * q_dotlcxterm_dir;

/* Options list */
static struct option_struct options[] = {

/* Directories */

        {Q_OPTION_DEFAULT_TEXT_COLOR, NULL, "default_text_color", "white, black", ""
"### TEXT COLOR ------------------------------------------------------------\n"
"\n"
"### The default console text color when the color is reset.  Specify it\n"
"### like one of the following two formats:\n"
"###\n"
"###   default_text_color = <foreground>, <background>\n"
"###   default_text_color = <foreground>, <background>, bold\n"
"###\n"
"### <foreground> and <background> can be one of the following:\n"
"###\n"
"###                red green blue yellow cyan magenta white black\n"
"###\n"
"###                grey and gray are treated as white\n"
"###                brown is treated as yellow\n"
"###"},

/* Directories */

        {Q_OPTION_WORKING_DIR, NULL, "working_dir", "$HOME", ""
"### DIRECTORIES -----------------------------------------------------------\n"
"\n"
"### The default working directory.  The $HOME environment variable will\n"
"### be substituted if specified."},

#ifndef Q_NO_PROTOCOLS
        {Q_OPTION_DOWNLOAD_DIR, NULL, "download_dir", "$HOME/Downloads", ""
"### The default directory to store downloaded files.  The $HOME\n"
"### environment variable will be substituted if specified."},

        {Q_OPTION_UPLOAD_DIR, NULL, "upload_dir", "$HOME", ""
"### The default directory to look for files to upload.  The $HOME\n"
"### environment variable will be substituted if specified."},

        {Q_OPTION_BATCH_ENTRY_FILE, NULL, "bew_file",
         "$HOME/.local/share/lcxterm/batch_upload.txt", ""
"### Where to store the Batch Entry Window entries."},
#endif

/* Spawned programs */

        /* Normal: use $SHELL */
        {Q_OPTION_SHELL, NULL, "shell", "$SHELL", ""
"### PROGRAMS -------------------------------------------------------------\n"
"\n"
"### The OS shell program.  The $SHELL environment  variable will be\n"
"### substituted if specified.  Examples: /bin/bash /bin/tcsh my_shell"},

        {Q_OPTION_EDITOR, NULL, "editor", "vi", ""
"### The editor program.  The $EDITOR environment variable will be\n"
"### substituted if specified."},

/* LANG flags */

        {Q_OPTION_ISO8859_LANG, NULL, "iso8859_lang", "C", ""
"### LANG ENVIRONMENT VARIABLE TO SEND ------------------------------------\n"
"\n"
"### The LANG environment variable to specify when using a non-\n"
"### Unicode emulation."},
        {Q_OPTION_UTF8_LANG, NULL, "utf8_lang", "en_US.UTF-8", ""
"### The LANG environment variable to specify for the XTERM\n"
"### and LINUX emulations."},

/* General flags */

         {Q_OPTION_XTERM_DOUBLE, NULL, "xterm_double_width", "true", ""
"### LCXTerm can display true double-width / double-height characters\n"
"### when run under an xterm that supports it.  Examples of xterms\n"
"### that can do so are PuTTY, Terminal.app on OS X, and of course\n"
"### the genuine XFree86 xterm ('xterm-new').\n"
"###\n"
"### Some programs known NOT to work are konsole, gnome-terminal,\n"
"### and rxvt."},

         {Q_OPTION_STATUS_LINE_VISIBLE, NULL, "status_line", "false", ""
"### Whether the status line is visible on startup.  Value is 'true' or\n"
"### 'false'."},

         {Q_OPTION_BRACKETED_PASTE, NULL, "bracketed_paste_mode", "false", ""
"### Whether or not bracketed paste mode is enabled.  Bracketed paste mode\n"
"### is a security feature that permits applications to distinguish between\n"
"### normal keyboard text and pasted text.  LCXTerm will automatically honor\n"
"### bracketed paste mode for XTERM and X_8BIT emulations.  Other\n"
"### emulations will only use bracketed paste mode if this value is\n"
"### 'true'."},

/* Capture file */

        {Q_OPTION_CAPTURE, NULL, "capture_enabled", "false", ""
"### CAPTURE FILE ---------------------------------------------------------\n"
"\n"
"### Whether or not capture is enabled on startup.  Value is\n"
"### 'true' or 'false'."},

        {Q_OPTION_CAPTURE_FILE, NULL, "capture_file", "capture.txt", ""
"### The default capture file name.  When enabled, all transmitted and\n"
"### received bytes (minus color) are appended to this file.  This file\n"
"### is stored in the working directory if a relative path is specified."},

        {Q_OPTION_CAPTURE_TYPE, NULL, "capture_type", "normal", ""
"### The default capture format.  Value is 'normal', 'raw', 'html', or\n"
"### 'ask'."},

/* Screen dump */

        {Q_OPTION_SCREEN_DUMP_TYPE, NULL, "screen_dump_type", "normal", ""
"### SCREEN DUMP ----------------------------------------------------------\n"
"\n"
"### The default screen dump format.  Value is 'normal', 'html', or\n"
"### 'ask'."},

/* Scrollback */

        {Q_OPTION_SCROLLBACK_LINES, NULL, "scrollback_max_lines", "20000", ""
"### SCROLLBACK BUFFER ----------------------------------------------------\n"
"\n"
"### The maximum number of lines to save in the scrollback buffer.  0 means\n"
"### unlimited scrollback."},

        {Q_OPTION_SCROLLBACK_SAVE_TYPE, NULL, "scrollback_save_type",
         "normal", ""
"### The default capture format.  Value is 'normal', 'html', or\n"
"### 'ask'."},

/* Doorway flags */

        {Q_OPTION_CONNECT_DOORWAY, NULL, "doorway_mode_on_connect", "off", ""
"### DOORWAY MODE ---------------------------------------------------------\n"
"\n"
"### Whether to automatically switch to DOORWAY or MIXED mode after\n"
"### connecting.  Value is 'doorway', 'mixed', or 'off'."},

        {Q_OPTION_DOORWAY_MIXED_KEYS, NULL, "doorway_mixed_mode_commands",
#ifdef Q_NO_PROTOCOLS
         "N P T Z /", ""
#else
         "N P T Z / PgUp PgDn", ""
#endif
"### A space-separated list of command keys that will be honored when in\n"
"### MIXED doorway mode.  Each key is one of the Alt-key combos on the Alt-Z\n"
"### Command menu, except for 'PgUp' and 'PgDn'.  Listing 'PgUp' or 'PgDn'\n"
"### here means to allow the unmodified 'PgUp' and 'PgDn' keys to go to the\n"
"### remote side but still honor ALT- and CTRL- 'PgUp' and 'PgDn'.\n"
"### The default commands to honor are:\n"
"###     Alt-P Capture\n"
"###     Alt-T Screen Dump\n"
"###     Alt-Z Menu\n"
"###     Alt-/ Scrollback"
#ifndef Q_NO_PROTOCOLS
"\n###     Alt-PgUp or Ctrl-PgUp Upload Files\n"
"###     Alt-PgDn or Ctrl-PgDn Download Files"
#endif
        },


/* Emulation: general */

        {Q_OPTION_ENQ_ANSWERBACK, NULL, "enq_response", "", ""
"### EMULATION: GENERAL ---------------------------------------------------\n"
"\n"
"### The string to respond with after receiving the ASCII ENQ (0x05, ^E).\n"
"### Value is a string.\n"
"###\n"
"### Many terminals can respond to a received ENQ with a user-provided\n"
"### string.  This was typically used for logging terminal identity and\n"
"### determining if it is still present.  Very few modern applications make\n"
"### use of this function, so most emulators return nothing (e.g. empty\n"
"### string)."},

/* Emulation: VT100 */

        {Q_OPTION_VT100_COLOR, NULL, "vt100_ansi_color", "true", ""
"### EMULATION: VT100 -----------------------------------------------------\n"
"\n"
"### Whether or not ANSI.SYS-style color selection commands will be\n"
"### supported with the VT100, VT102, and VT220 emulations.  Value is\n"
"### 'true' or 'false'.\n"
"###\n"
"### Real VT100, VT102, and VT220 applications are in black and white\n"
"### only.  However, some host applications send color selection commands\n"
"### despite the fact the termcap/terminfo entry lacks these codes.\n"
"### If this value is set to true the VT100, VT102, and VT220 emulation\n"
"### will honor the color selection codes.  If this value is false the\n"
"### color selection codes will be quietly consumed, as a real VT100-ish\n"
"### terminal would do."},

         {Q_OPTION_XTERM_MOUSE_REPORTING, NULL, "xterm_mouse_reporting", "true", ""
"### EMULATION: XTERM -----------------------------------------------------\n"
"\n"
"### Whether to support xterm mouse reporting at all.  Note that this only\n"
"### affects XTERM and X_8BIT emulations.  Mouse reporting is never sent to\n"
"### the remote side for non-xterm emulations.  Value is 'true' or 'false'."},

#ifndef Q_NO_PROTOCOLS
/* File transfer protocol: ASCII */

        {Q_OPTION_ASCII_UPLOAD_CR_POLICY, NULL, "ascii_upload_cr_policy",
         "none", ""
"### FILE TRANSFER PROTOCOL: ASCII ----------------------------------------\n"
"\n"
"### How to handle outgoing carriage-return characters (0x0D)\n"
"### during ASCII file uploads.  Value is 'none', 'strip', or 'add'.\n"
"###\n"
"### 'none' means do nothing to change the bytes sent.\n"
"### 'strip' means remove carriage-returns while sending the file.\n"
"### 'add' means add a linefeed character (0x0A) after each carriage-"
"return\n"
"### while sending the file."},

        {Q_OPTION_ASCII_UPLOAD_LF_POLICY, NULL, "ascii_upload_lf_policy",
         "none", ""
"### How to handle outgoing linefeed characters (0x0A) during ASCII file\n"
"### uploads.  Value is 'none', 'strip', or 'add'.\n"
"###\n"
"### 'none' means do nothing to change the bytes sent.\n"
"### 'strip' means remove linefeeds while sending the file.\n"
"### 'add' means add a carriage-return character (0x0D) before each "
"linefeed\n"
"### while sending the file."},

        {Q_OPTION_ASCII_DOWNLOAD_CR_POLICY, NULL, "ascii_download_cr_policy",
         "none", ""
"### How to handle incoming carriage-return characters (0x0D)\n"
"### during ASCII file downloads.  Value is 'none', 'strip', or 'add'.\n"
"###\n"
"### 'none' means do nothing to change the bytes saved.\n"
"### 'strip' means remove carriage-returns while saving the file.\n"
"### 'add' means add a linefeed character (0x0A) after each carriage-"
"return\n"
"### while saving the file."},

        {Q_OPTION_ASCII_DOWNLOAD_LF_POLICY, NULL, "ascii_download_lf_policy",
         "none", ""
"### How to handle incoming linefeed characters (0x0A) during ASCII file\n"
"### downloads.  Value is 'none', 'strip', or 'add'.\n"
"###\n"
"### 'none' means do nothing to change the bytes saved.\n"
"### 'strip' means remove linefeeds while saving the file.\n"
"### 'add' means add a carriage-return character (0x0D) before each "
"linefeed\n"
"### while saving the file."},
#endif /* Q_NO_PROTOCOLS */

#ifndef Q_NO_ZMODEM
/* File transfer protocol: ZMODEM */

        {Q_OPTION_ZMODEM_AUTOSTART, NULL, "zmodem_autostart", "true", ""
"### FILE TRANSFER PROTOCOL: ZMODEM ---------------------------------------\n"
"\n"
"### Whether or not Zmodem autostart should be used.\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means Zmodem autostart will be enabled.\n"
"### 'false' means Zmodem autostart will not be enabled."},

        {Q_OPTION_ZMODEM_ZCHALLENGE, NULL, "zmodem_zchallenge", "false", ""
"### Whether or not Zmodem will issue ZCHALLENGE at the beginning.\n"
"### of each transfer.  ZCHALLENGE was meant to improve security\n"
"### but some Zmodem clients do not support it.  Its security\n"
"### benefits are dubious.\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means Zmodem will issue a ZCHALLENGE.\n"
"### 'false' means Zmodem will not issue a ZCHALLENGE."},

        {Q_OPTION_ZMODEM_ESCAPE_CTRL, NULL, "zmodem_escape_control_chars",
         "false", ""
"### Whether or not Zmodem should escape control characters by default.\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means Zmodem will escape control characters, which will\n"
"### make file transfers slower but may be necessary for Zmodem to\n"
"### work at all over the link.\n"
"### 'false' means Zmodem will not escape control characters.\n"
"### \n"
"### In both cases, Zmodem will honor the encoding requested at the\n"
"### other end.\n"
"### \n"
"### The Zmodem protocol defines this as an available option, and LCXTerm\n"
"### supports it for completeness.  However, (l)rzsz never used it, and\n"
"### enabling this option causes uploads to (l)rzsz to fail.\n"
"### \n"
"### DO NOT SET THIS TO 'true' UNLESS YOU ARE TESTING ANOTHER ZMODEM\n"
"### IMPLEMENTATION."},
#endif /* Q_NO_ZMODEM */

#ifndef Q_NO_KERMIT
/* File transfer protocol: KERMIT */

        {Q_OPTION_KERMIT_AUTOSTART, NULL, "kermit_autostart", "true", ""
"### FILE TRANSFER PROTOCOL: KERMIT ---------------------------------------\n"
"\n"
"### Whether or not Kermit autostart should be enabled by default.\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means Kermit autostart will be enabled on startup.\n"
"### 'false' means Kermit autostart will not be enabled on startup."},

        {Q_OPTION_KERMIT_ROBUST_FILENAME, NULL, "kermit_robust_filename",
         "false", ""
"### Whether or not Kermit should use common form filenames.\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means Kermit uploads will convert filenames to uppercase,\n"
"### remove all but one period, and remove many punctuation characters.\n"
"### 'false' means Kermit uplods will use the literal filename."},

        {Q_OPTION_KERMIT_STREAMING, NULL, "kermit_streaming", "true", ""
"### Whether or not Kermit should use streaming (sending all file data\n"
"### packets continuously without waiting for ACKs).\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means Kermit will use streaming, resulting in a significant\n"
"### performance improvement in most cases, especially over TCP links.\n"
"### 'false' means Kermit will not use streaming."},

        {Q_OPTION_KERMIT_UPLOADS_FORCE_BINARY, NULL,
         "kermit_uploads_force_binary", "true", ""
"### Whether or not Kermit uploads will transfer files as 8-bit binary files.\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means Kermit uploads will transfer all files (including text\n"
"### files) in binary.\n"
"### 'false' means Kermit will convert text files to CRLF format, but\n"
"### leave binary files as-is.  Note that LCXTerm's kermit checks the first\n"
"### 1024 bytes of the file, and if it sees only ASCII characters assumes\n"
"### the file is text; this heuristic might occasionally mis-identify files."},

        {Q_OPTION_KERMIT_DOWNLOADS_CONVERT_TEXT, NULL,
         "kermit_downloads_convert_text", "false", ""
"### Whether or not Kermit downloads will convert text files to the local\n"
"### end-of-line convention (e.g. CRLF -> LF).\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means Kermit downloads will convert CRLF to LF.\n"
"### 'false' means Kermit will leave text files in the format sent, usually\n"
"### CRLF."},

        {Q_OPTION_KERMIT_RESEND, NULL, "kermit_resend", "true", ""
"### Whether or not Kermit uploads should RESEND by default.  The RESEND\n"
"### option appends data to existing files.  Most of the time this results\n"
"### file transfers resuming where they left off, similar to Zmodem crash\n"
"### recovery.\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means all Kermit uploads will use RESEND.\n"
"### 'false' means Kermit uploads will use SEND."},

         {Q_OPTION_KERMIT_LONG_PACKETS, NULL, "kermit_long_packets", "true", ""
"### Whether or not Kermit should use long packets.  On very noisy channels,\n"
"### Kermit may need to use short packets to get through.\n"
"### Value is 'true' or 'false'.\n"
"###\n"
"### 'true' means Kermit will use long packets, up to 1k.\n"
"### 'false' means Kermit will use short packets, up to 96 bytes."},
#endif /* Q_NO_KERMIT */

    {Q_OPTION_NULL, NULL, NULL, NULL}
};

/**
 * Replace all instances of "pattern" in "original" with "new_string",
 * returning a newly-allocated string.
 *
 * @param original the original string
 * @param pattern the pattern in original string to replace
 * @param new_string the string to replace pattern with
 * @return a newly-allocated string
 */
char * substitute_string(const char * original, const char * pattern,
                         const char * new_string) {
    int i;
    /*
     * Cast to avoid compiler warning
     */
    char * current_place = (char *) original;
    char * old_place;
    char * return_string;
    int return_string_place;

    /*
     * Count all occurrences
     */
    i = 0;
    while (current_place[0] != '\0') {
        current_place = strstr(current_place, pattern);
        if (current_place == NULL) {
            break;
        } else {
            i++;
            current_place += strlen(pattern);
        }
    }

    /*
     * i contains the # of occurrences of pattern.
     */
    return_string =
        (char *) Xmalloc(1 + strlen(original) +
                         (i * (strlen(new_string) - strlen(pattern))), __FILE__,
                         __LINE__);
    memset(return_string, 0,
           1 + strlen(original) + (i * (strlen(new_string) - strlen(pattern))));
    /*
     * Cast to avoid compiler warning
     */
    current_place = (char *) original;
    old_place = current_place;
    return_string_place = 0;
    while (i > 0) {
        current_place = strstr(old_place, pattern);
        /*
         * Copy part before pattern
         */
        strncpy(return_string + return_string_place, old_place,
                current_place - old_place);
        return_string_place += current_place - old_place;
        old_place = current_place;

        /*
         * Copy new string
         */

        /*
         * GCC does not like the way strncpy uses new_string's length.  But
         * it is safe here because return_string was reallocated to have
         * enough space for it.
         */
#pragma GCC diagnostic ignored "-Wstringop-overflow"

        strncpy(return_string + return_string_place, new_string,
                strlen(new_string));
        return_string_place += strlen(new_string);
        old_place += strlen(pattern);
        current_place = old_place;
        i--;
    }
    /*
     * Copy terminating non-pattern part
     */
    strncpy(return_string + return_string_place, current_place,
            original + strlen(original) - current_place);

    return return_string;
}

/**
 * Replace all instances of "pattern" in "original" with "new_string",
 * returning a newly-allocated string.
 *
 * @param original the original string
 * @param pattern the pattern in original string to replace
 * @param new_string the string to replace pattern with
 * @return a newly-allocated string
 */
wchar_t * substitute_wcs(const wchar_t * original, const wchar_t * pattern,
                         const wchar_t * new_string) {
    int i;
    /*
     * Cast to avoid compiler warning
     */
    wchar_t * current_place = (wchar_t *) original;
    wchar_t * old_place;
    wchar_t * return_string;
    int return_string_place;

    /*
     * Count all occurrences
     */
    i = 0;
    while (current_place[0] != L'\0') {
        current_place = wcsstr(current_place, pattern);
        if (current_place == NULL) {
            break;
        } else {
            i++;
            current_place += wcslen(pattern);
        }
    }

    /*
     * i contains the # of occurrences of pattern.
     */
    return_string =
        (wchar_t *) Xmalloc((sizeof(wchar_t)) *
                            (1 + wcslen(original) +
                             (i * (wcslen(new_string) - wcslen(pattern)))),
                            __FILE__, __LINE__);
    wmemset(return_string, 0,
            1 + wcslen(original) +
            (i * (wcslen(new_string) - wcslen(pattern))));
    /*
     * Cast to avoid compiler warning
     */
    current_place = (wchar_t *) original;
    old_place = current_place;
    return_string_place = 0;
    while (i > 0) {
        current_place = wcsstr(old_place, pattern);
        /*
         * Copy part before pattern
         */
        wcsncpy(return_string + return_string_place, old_place,
                current_place - old_place);
        return_string_place += current_place - old_place;
        old_place = current_place;
        /*
         * Copy new string
         */
        wcsncpy(return_string + return_string_place, new_string,
                wcslen(new_string));
        return_string_place += wcslen(new_string);
        old_place += wcslen(pattern);
        current_place = old_place;
        i--;
    }
    /*
     * Copy terminating non-pattern part
     */
    wcsncpy(return_string + return_string_place, current_place,
            original + wcslen(original) - current_place);

    return return_string;
}

/**
 * Replace all instances of "pattern" in "original" with "new_string",
 * returning a newly-allocated string.
 *
 * @param original the original string
 * @param pattern the pattern in original string to replace
 * @param new_string the string to replace pattern with.  It will be
 * converted to UTF-8.
 * @return a newly-allocated string
 */
char * substitute_wcs_half(const char * original, const char * pattern,
                           const wchar_t * new_string) {
    int i;
    /*
     * Cast to avoid compiler warning
     */
    char * current_place = (char *) original;
    char * old_place;
    char * return_string;
    int return_string_place;
    char new_str[256];

    /*
     * Convert to UTF-8
     */
    memset(new_str, 0, sizeof(new_str));
    wcstombs(new_str, new_string, wcslen(new_string));

    /*
     * Count all occurrences
     */
    i = 0;
    while (current_place[0] != '\0') {
        current_place = strstr(current_place, pattern);
        if (current_place == NULL) {
            break;
        } else {
            i++;
            current_place++;
        }
    }

    /*
     * i contains the # of occurrences of pattern.
     */
    return_string =
        (char *) Xmalloc(1 + strlen(original) +
                         (i * (strlen(new_str) - strlen(pattern))), __FILE__,
                         __LINE__);
    memset(return_string, 0,
           1 + strlen(original) + (i * (strlen(new_str) - strlen(pattern))));
    /*
     * Cast to avoid compiler warning
     */
    current_place = (char *) original;
    old_place = current_place;
    return_string_place = 0;
    while (i > 0) {
        current_place = strstr(old_place, pattern);
        /*
         * Copy part before pattern
         */
        strncpy(return_string + return_string_place, old_place,
                current_place - old_place);
        return_string_place += current_place - old_place;
        old_place = current_place;
        /*
         * Copy new string
         */
        strncpy(return_string + return_string_place, new_str, strlen(new_str));
        return_string_place += strlen(new_str);
        old_place += strlen(pattern);
        current_place = old_place;
        i--;
    }
    /*
     * Copy terminating non-pattern part
     */
    strncpy(return_string + return_string_place, current_place,
            original + strlen(original) - current_place);

    return return_string;
}

/**
 * Get an option value.  Note that the string returned is not
 * newly-allocated, i.e. do not free it later.
 *
 * @param option the option
 * @return the option value from the config file
 */
char * get_option(const Q_OPTION option) {
    struct option_struct * current_option = options;
    while (current_option->option != Q_OPTION_NULL) {
        if (current_option->option == option) {
            return current_option->value;
        }
        current_option++;
    }
    return "";
}

/**
 * Get the long description for an option.  The help system uses this to
 * automatically generate a help screen out of the options descriptions.
 *
 * @param option the option
 * @return the option description
 */
const char * get_option_description(const Q_OPTION option) {
    struct option_struct * current_option = options;
    while (current_option->option != Q_OPTION_NULL) {
        if (current_option->option == option) {
            return current_option->comment;
        }
        current_option++;
    }
    return "";
}

/**
 * Get the key for an option.  The help system uses this to automatically
 * generate a help screen out of the options descriptions.
 *
 * @param option the option
 * @return the option key
 */
const char * get_option_key(const Q_OPTION option) {
    struct option_struct * current_option = options;
    while (current_option->option != Q_OPTION_NULL) {
        if (current_option->option == option) {
            return current_option->name;
        }
        current_option++;
    }
    return "";
}

/**
 * Get the default value for an option.  The help system uses this to
 * automatically generate a help screen out of the options descriptions.
 *
 * @param option the option
 * @return the option default value
 */
const char * get_option_default(const Q_OPTION option) {
    struct option_struct * current_option = options;
    while (current_option->option != Q_OPTION_NULL) {
        if (current_option->option == option) {
            return current_option->default_value;
        }
        current_option++;
    }
    return "";
}

/**
 * Save options to a file.
 *
 * @param filename file to save to
 * @return true if successful
 */
Q_BOOL save_options(const char * filename) {
    struct option_struct * current_option;
    FILE * file;
    int rc;
    char time_string[TIME_STRING_LENGTH];
    time_t current_time;

    if (q_status.read_only == Q_TRUE) {
        return Q_FALSE;
    }

    file = fopen(filename, "w");
    if (file == NULL) {
        fprintf(stderr, _("Error opening file \"%s\" for writing: %s"),
                filename, strerror(errno));
        return Q_FALSE;
    }

    /*
     * Standard header.
     */
    time(&current_time);
    strftime(time_string, sizeof(time_string), _("%a, %d %b %Y %H:%M:%S %z"),
        localtime(&current_time));
    fprintf(file, _("### LCXTerm " Q_VERSION " options file generated %s\n"),
        time_string);
    fprintf(file, "###\n");
    fprintf(file, "### This file is typically at "
        "~/.local/share/lcxterm/lcxtermrc\n");
    fprintf(file, "### ------------------------------------------------------"
        "-----------------");
    fprintf(file, "\n\n");

    /*
     * Emit each option, its description and default value.
     */
    current_option = options;
    while (current_option->option != Q_OPTION_NULL) {
        rc = fwrite(current_option->comment, strlen(current_option->comment), 1,
                    file);
        if (rc == -1) {
            fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                    strerror(errno));
            return Q_FALSE;
        }
        rc = fwrite("\n###", 4, 1, file);
        if (rc == -1) {
            fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                    strerror(errno));
            return Q_FALSE;
        }
        rc = fprintf(file, _("\n### Default value: "));
        if (rc == -1) {
            fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                    strerror(errno));
            return Q_FALSE;
        }
        rc = fwrite(current_option->default_value,
                    strlen(current_option->default_value), 1, file);
        if (rc == -1) {
            fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                    strerror(errno));
            return Q_FALSE;
        }
        if (strncmp
            (current_option->value, current_option->default_value,
             strlen(current_option->default_value)) == 0) {
            rc = fwrite("\n### ", 5, 1, file);
            if (rc == -1) {
                fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                        strerror(errno));
                return Q_FALSE;
            }
        } else {
            rc = fwrite("\n", 1, 1, file);
            if (rc == -1) {
                fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                        strerror(errno));
                return Q_FALSE;
            }
        }
        rc = fwrite(current_option->name, strlen(current_option->name), 1,
                    file);
        if (rc == -1) {
            fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                    strerror(errno));
            return Q_FALSE;
        }
        rc = fwrite(" = ", 3, 1, file);
        if (rc == -1) {
            fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                    strerror(errno));
            return Q_FALSE;
        }
        rc = fwrite(current_option->value, strlen(current_option->value), 1,
                    file);
        if (rc == -1) {
            fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                    strerror(errno));
            return Q_FALSE;
        }
        rc = fwrite("\n\n\n", 3, 1, file);
        if (rc == -1) {
            fprintf(stderr, _("Error writing to file \"%s\": %s"), filename,
                    strerror(errno));
            return Q_FALSE;
        }

        current_option++;
    }

    fclose(file);

    return Q_TRUE;
}

/**
 * Set an option's value.  This allocates a new copy of value.
 *
 * @param option the option to set
 * @param value the new value
 */
static void set_option(struct option_struct * option, char * value) {
    char * new_value = value;
    while (((q_isspace(new_value[0])) || (new_value[0] == '='))
           && (strlen(new_value) > 0)) {
        new_value++;
    }
    new_value = Xstrdup(new_value, __FILE__, __LINE__);;
    if (option->value != NULL) {
        Xfree(option->value, __FILE__, __LINE__);
    }
    option->value = new_value;
}

/**
 * Perform option-specific substitutions for $HOME and $EDITOR.
 *
 * @param option the option to check
 */
static void check_option(struct option_struct * option) {
    char * env_string;
    char * new_value;

    /*
     * Set a default value to empty string so our calls to
     * substitute_string() can have an Xfree() call.
     */
    if (option->value == NULL) {
        option->value = Xstrdup("", __FILE__, __LINE__);
    }

    switch (option->option) {

    case Q_OPTION_WORKING_DIR:
#ifndef Q_NO_PROTOCOLS
    case Q_OPTION_BATCH_ENTRY_FILE:
    case Q_OPTION_UPLOAD_DIR:
    case Q_OPTION_DOWNLOAD_DIR:
#endif
        env_string = get_home_directory();
        if (env_string == NULL) {
            env_string = "";
        }
        /*
         * Sustitute for $HOME
         */
        new_value = substitute_string(option->value, "$HOME", env_string);
        Xfree(option->value, __FILE__, __LINE__);
        option->value = new_value;
        break;
    case Q_OPTION_EDITOR:
        env_string = getenv("EDITOR");
        if (env_string == NULL) {
            env_string = "";
        }
        /*
         * Sustitute for $EDITOR
         */
        new_value = substitute_string(option->value, "$EDITOR", env_string);
        Xfree(option->value, __FILE__, __LINE__);
        option->value = new_value;
        break;
    case Q_OPTION_SHELL:
        env_string = getenv("SHELL");
        if (env_string == NULL) {
            env_string = "";
        }
        /*
         * Sustitute for $EDITOR
         */
        new_value = substitute_string(option->value, "$SHELL", env_string);
        Xfree(option->value, __FILE__, __LINE__);
        option->value = new_value;
        break;
    default:
        break;
    }
}

/**
 * Load options from a file.
 *
 * @param filename file to read from
 */
static void load_options_from_file(const char * filename) {
    struct option_struct * current_option;
    FILE * file;
    char line[OPTIONS_LINE_SIZE];
    char * line_begin;

    file = fopen(filename, "r");
    if (file == NULL) {
        fprintf(stderr, _("Error opening file \"%s\" for reading: %s"),
                filename, strerror(errno));
        return;
    }

    while (!feof(file)) {
        memset(line, 0, sizeof(line));
        if (fgets(line, sizeof(line), file) == NULL) {
            if (!feof(file)) {
                fprintf(stderr, _("Error reading from file \"%s\": %s"),
                        filename, strerror(errno));
            }
            break;
        }

        line_begin = line;

        /*
         * Trim whitespace from line
         */
        while ((strlen(line_begin) > 0)
               && q_isspace(line_begin[strlen(line_begin) - 1])) {
            line_begin[strlen(line_begin) - 1] = '\0';
        }
        while ((q_isspace(*line_begin)) && (strlen(line_begin) > 0)) {
            line_begin++;
        }

        if (*line_begin == '#') {
            /*
             * Skip comment lines
             */
            continue;
        }

        /*
         * Look for option this line changes
         */
        current_option = options;
        while (current_option->option != Q_OPTION_NULL) {
            if (strstr(line, current_option->name) == line) {
                if ((line[strlen(current_option->name)] == '=') ||
                    (q_isspace(line[strlen(current_option->name)]))) {

                    /*
                     * Valid option
                     */
                    set_option(current_option,
                               line + strlen(current_option->name));
                }
            }
            check_option(current_option);
            current_option++;
        }
    }

    fclose(file);
}

/**
 * Get the full path to the options config file.
 *
 * @return the full path to lcxtermrc (usually
 * ~/.local/share/lcxterm/lcxtermrc).
 */
char * get_options_filename() {
    return home_directory_options_filename;
}

/**
 * Find the option_struct from the Q_OPTION value.
 *
 * @param option the option enum
 * @return the options struct
 */
static struct option_struct * find_option(const Q_OPTION option) {
    struct option_struct * current_option;

    current_option = options;
    while (current_option->option != Q_OPTION_NULL) {
        if (current_option->option == option) {
            return current_option;
        }
        current_option++;
    }
    return NULL;
}

/**
 * Create a directory.  This will also create any directories that are
 * missing in the middle.
 *
 * @param path the directory path name
 * @return true if successful
 */
static Q_BOOL create_directory(const char * path) {
    char * path_copy;
    char * parent_dir;
    int i;

    if (q_status.read_only == Q_TRUE) {
        return Q_FALSE;
    }

    assert(directory_exists(path) == Q_FALSE);
    if (file_exists(path) == Q_TRUE) {
        /*
         * We cannot create a directory when a file already exists there.
         */
        return Q_FALSE;
    }

    path_copy = Xstrdup(path, __FILE__, __LINE__);
    parent_dir = Xstrdup(dirname(path_copy), __FILE__, __LINE__);
    if (directory_exists(parent_dir) == Q_FALSE) {
        create_directory(parent_dir);
    }
    Xfree(parent_dir, __FILE__, __LINE__);
    Xfree(path_copy, __FILE__, __LINE__);

    i = mkdir(path, S_IRUSR | S_IWUSR | S_IXUSR);
    if (i == 0) {
        return Q_TRUE;
    } else {
        return Q_FALSE;
    }
}

/**
 * Create everything that needs to be in ~/.local/share/lcxterm.
 */
static void populate_dotlcxterm() {
#ifndef Q_NO_KEYMACROS
    /*
     * Create the key bindings files
     */
    create_keybindings_files();
#endif
}

/**
 * Create everything that needs to be in ~/lcxterm.
 */
static void check_and_create_directories() {
    Q_BOOL rc;
    char * working_dir;
    char notify_message[DIALOG_MESSAGE_SIZE];
    char * message_lines[10];
    char * env_string;

    /*
     * We will use $HOME several times now, hang onto it.
     */
    env_string = get_home_directory();

    /*
     * Check for working directory
     */
    working_dir =
        substitute_string(get_option(Q_OPTION_WORKING_DIR), "$HOME",
                          env_string);

    if (directory_exists(working_dir) == Q_FALSE) {
        rc = create_directory(working_dir);
        if (rc == Q_FALSE) {
            snprintf(notify_message, sizeof(notify_message), _(""
                    "Could not create the directory %s."), working_dir);
            message_lines[0] = notify_message;
            message_lines[1] = _("You may have to specify full paths when you");
            message_lines[2] = _("download files, enable capture/log, etc."),
            notify_form_long(message_lines, 0, 3);
        }
    }
    /*
     * Free leak
     */
    Xfree(working_dir, __FILE__, __LINE__);

}

/**
 * Reset options to default state.
 */
void reset_options() {
    struct option_struct * current_option;
    current_option = options;
    while (current_option->option != Q_OPTION_NULL) {
        if (current_option->value != NULL) {
            Xfree(current_option->value, __FILE__, __LINE__);
        }
        current_option->value =
            Xstrdup(current_option->default_value, __FILE__, __LINE__);

        /*
         * Translate option help text to local language
         */
        current_option->comment = _(current_option->comment);
        current_option++;
    }
}

/**
 * This must be called to initialize the options list from the config file.
 *
 * Load options from all the files.  We search the following:
 *     $HOME/.local/share/lcxterm/lcxtermrc
 *     $HOME/lcxterm/.lcxtermrc
 *     INSTALL_DIR/lcxtermrc
 *     /etc/lcxtermrc
 *     /usr/lib/lcxterm/lcxtermrc
 *     /usr/local/lib/lcxterm/lcxtermrc
 */
void load_options() {
    int i;
    Q_BOOL rc;
    char * filenames[] = {
        INSTALL_DIR "/lcxtermrc",
        "/etc/lcxtermrc",
        "/usr/lib/lcxterm/lcxtermrc",
        "/usr/local/lib/lcxterm/lcxtermrc",

        /*
         * List $HOME last so that it overrides everything else.
         */
        "$HOME/.lcxterm/lcxtermrc",
        "$HOME/.local/share/lcxterm/lcxtermrc",
        NULL
    };
    char * current_filename;
    char * env_string;
    char * substituted_filename;
    char * lang_default;
    char notify_message[DIALOG_MESSAGE_SIZE];
    char * message_lines[10];

    /*
     * Set default values.
     */
    reset_options();

    /*
     * It is main()'s job to set q_home_directory before calling
     * load_options().  Make sure that was done.
     */
    assert(q_home_directory != NULL);

    /*
     * Check for .lcxterm directory
     */
    if (directory_exists(q_home_directory) == Q_FALSE) {
        rc = create_directory(q_home_directory);
        if (rc == Q_FALSE) {
            snprintf(notify_message, sizeof(notify_message), _(""
                    "Could not create the directory %s."), q_home_directory);
            message_lines[0] = notify_message;
            message_lines[1] = _("You may have to specify full paths when");
            message_lines[2] = _("you load key bindings, phone books, etc."),
            notify_form_long(message_lines, 0, 3);
        } else {
            /*
             * Since we just created .lcxterm, we can now create everything in
             * it.
             */
            populate_dotlcxterm();
        }
    }

    /*
     * Special check: $HOME/.local/share/lcxterm/lcxtermrc.  If this doesn't
     * exist, AND we just created its directory, then we create it here
     * before moving on.
     */
    substituted_filename =
        substitute_string("$HOME/lcxtermrc", "$HOME", q_home_directory);
    if (file_exists(substituted_filename) == Q_FALSE) {
        /*
         * Set the ISO8859 and UTF8 lang variables in the new lcxtermrc based
         * on the LANG environment variable.
         */
        lang_default = getenv("LANG");
        if (lang_default == NULL) {
            /*
             * User doesn't have it set, so we use the defaults.
             */
        } else {
            if (strstr(lang_default, "UTF-8") == NULL) {
                /*
                 * User has LANG set, but it isn't UTF-8.  Use LANG for
                 * 8-bit, and default for UTF-8.
                 */
                set_option(find_option(Q_OPTION_ISO8859_LANG), lang_default);
            } else {
                /*
                 * User has LANG set, and it is UTF-8.  Use LANG for UTF-8,
                 * and default for 8-bit.
                 */
                set_option(find_option(Q_OPTION_UTF8_LANG), lang_default);
            }
        }

        /*
         * Save options
         */
        if (save_options(substituted_filename) == Q_FALSE) {
            snprintf(notify_message, sizeof(notify_message),
                _("Error saving default options to %s"), substituted_filename);
            notify_form(notify_message, 0);
        }
    }
    /*
     * Free leak
     */
    Xfree(substituted_filename, __FILE__, __LINE__);

    /*
     * We will use $HOME several times now, hang onto it.
     */
    env_string = get_home_directory();

    if (q_config_filename != NULL) {
        /*
         * The user specified --config on the command line.  Only read that
         * file, don't read any of the others.
         */
        if (access(q_config_filename, F_OK | R_OK) == 0) {
            load_options_from_file(q_config_filename);
        }
        goto no_more_config_files;
    }

    /*
     * Read options from all the available files, overwriting them with each
     * read.
     */
    i = 0;
    for (current_filename = filenames[i]; current_filename != NULL;
         i++, current_filename = filenames[i]) {
        /*
         * Sustitute for $HOME
         */
        substituted_filename =
            substitute_string(current_filename, "$HOME", env_string);

        /*
         * Save that last filename so we can use it for Alt-N Configuration
         * later.
         */
        if (filenames[i + 1] == NULL) {
            sprintf(home_directory_options_filename, "%s",
                    substituted_filename);
        }

        /*
         * Check existence of file and readability.
         */
        if (access(substituted_filename, F_OK | R_OK) == 0) {
            load_options_from_file(substituted_filename);
        }
        /*
         * Free leak
         */
        Xfree(substituted_filename, __FILE__, __LINE__);

    }

no_more_config_files:

    /*
     * ----------------------------------------------------------------------
     *
     * At this point, we have a ~/.lcxterm created, a ~/.lcxterm/lcxtermrc
     * created, and we have read all of the options between
     * INSTALL_DIR/lcxtermrc and $HOME/.lcxterm/lcxtermrc.  The working, host
     * mode, and script directory names are now known.
     *
     * Create the directories if needed.  Then wrap up the options handling
     * and be done.
     *
     * ----------------------------------------------------------------------
     */
    check_and_create_directories();

    /*
     * Special-case options.  For each one, reset to default and then re-load
     * from file.
     */

    q_status.bracketed_paste_mode = Q_FALSE;
    if (strcasecmp(get_option(Q_OPTION_BRACKETED_PASTE), "true") == 0) {
        q_status.bracketed_paste_mode = Q_TRUE;
    }

    q_scrollback_max = atoi(get_option_default(Q_OPTION_SCROLLBACK_LINES));
    if (get_option(Q_OPTION_SCROLLBACK_LINES) != NULL) {
        q_scrollback_max = atoi(get_option(Q_OPTION_SCROLLBACK_LINES));
    }
    if (q_scrollback_max < 0) {
        q_scrollback_max = 0;
    }

    /*
     * Capture types
     */
    reset_capture_type();
    reset_screen_dump_type();
    reset_scrollback_save_type();

#ifndef Q_NO_ZMODEM
    q_status.zmodem_autostart = Q_TRUE;
    if (strcasecmp(get_option(Q_OPTION_ZMODEM_AUTOSTART), "false") == 0) {
        q_status.zmodem_autostart = Q_FALSE;
    }
    q_status.zmodem_zchallenge = Q_FALSE;
    if (strcasecmp(get_option(Q_OPTION_ZMODEM_ZCHALLENGE), "true") == 0) {
        q_status.zmodem_zchallenge = Q_TRUE;
    }
    q_status.zmodem_escape_ctrl = Q_FALSE;
    if (strcasecmp(get_option(Q_OPTION_ZMODEM_ESCAPE_CTRL), "true") == 0) {
        q_status.zmodem_escape_ctrl = Q_TRUE;
    }
#endif /* Q_NO_ZMODEM */

#ifndef Q_NO_KERMIT
    q_status.kermit_autostart = Q_TRUE;
    if (strcasecmp(get_option(Q_OPTION_KERMIT_AUTOSTART), "false") == 0) {
        q_status.kermit_autostart = Q_FALSE;
    }
    q_status.kermit_robust_filename = Q_FALSE;
    if (strcasecmp(get_option(Q_OPTION_KERMIT_ROBUST_FILENAME), "true") == 0) {
        q_status.kermit_robust_filename = Q_TRUE;
    }
    q_status.kermit_streaming = Q_TRUE;
    if (strcasecmp(get_option(Q_OPTION_KERMIT_STREAMING), "false") == 0) {
        q_status.kermit_streaming = Q_FALSE;
    }
    q_status.kermit_long_packets = Q_TRUE;
    if (strcasecmp(get_option(Q_OPTION_KERMIT_LONG_PACKETS), "false") == 0) {
        q_status.kermit_long_packets = Q_FALSE;
    }
    q_status.kermit_uploads_force_binary = Q_TRUE;
    if (strcasecmp(get_option(Q_OPTION_KERMIT_UPLOADS_FORCE_BINARY), "false") ==
        0) {
        q_status.kermit_uploads_force_binary = Q_FALSE;
    }
    q_status.kermit_downloads_convert_text = Q_FALSE;
    if (strcasecmp(get_option(Q_OPTION_KERMIT_DOWNLOADS_CONVERT_TEXT), "true")
        == 0) {
        q_status.kermit_downloads_convert_text = Q_TRUE;
    }
    q_status.kermit_resend = Q_TRUE;
    if (strcasecmp(get_option(Q_OPTION_KERMIT_RESEND), "false") == 0) {
        q_status.kermit_resend = Q_FALSE;
    }
#endif /* Q_NO_KERMIT */

    q_status.xterm_double = Q_TRUE;
    if (strcasecmp(get_option(Q_OPTION_XTERM_DOUBLE), "false") == 0) {
        q_status.xterm_double = Q_FALSE;
    }
    q_status.xterm_mouse_reporting = Q_TRUE;
    if (strcasecmp(get_option(Q_OPTION_XTERM_MOUSE_REPORTING), "false") == 0) {
        q_status.xterm_mouse_reporting = Q_FALSE;
    }
    q_status.vt100_color = Q_TRUE;
    if (strcasecmp(get_option(Q_OPTION_VT100_COLOR), "false") == 0) {
        q_status.vt100_color = Q_FALSE;
    }

}

/**
 * Set q_status.capture_type to whatever is defined in the options file.
 */
void reset_capture_type() {
    q_status.capture_type = Q_CAPTURE_TYPE_NORMAL;
    if (strcasecmp(get_option(Q_OPTION_CAPTURE_TYPE), "raw") == 0) {
        q_status.capture_type = Q_CAPTURE_TYPE_RAW;
    }
    if (strcasecmp(get_option(Q_OPTION_CAPTURE_TYPE), "html") == 0) {
        q_status.capture_type = Q_CAPTURE_TYPE_HTML;
    }
    if (strcasecmp(get_option(Q_OPTION_CAPTURE_TYPE), "ask") == 0) {
        q_status.capture_type = Q_CAPTURE_TYPE_ASK;
    }
}

/**
 * Set q_status.screen_dump_type to whatever is defined in the options file.
 */
void reset_screen_dump_type() {
    q_status.screen_dump_type = Q_CAPTURE_TYPE_NORMAL;
    if (strcasecmp(get_option(Q_OPTION_SCREEN_DUMP_TYPE), "html") == 0) {
        q_status.screen_dump_type = Q_CAPTURE_TYPE_HTML;
    }
    if (strcasecmp(get_option(Q_OPTION_SCREEN_DUMP_TYPE), "ask") == 0) {
        q_status.screen_dump_type = Q_CAPTURE_TYPE_ASK;
    }
}

/**
 * Set q_status.scrollback_save_type to whatever is defined in the options
 * file.
 */
void reset_scrollback_save_type() {
    q_status.scrollback_save_type = Q_CAPTURE_TYPE_NORMAL;
    if (strcasecmp(get_option(Q_OPTION_SCROLLBACK_SAVE_TYPE), "html") == 0) {
        q_status.scrollback_save_type = Q_CAPTURE_TYPE_HTML;
    }
    if (strcasecmp(get_option(Q_OPTION_SCROLLBACK_SAVE_TYPE), "ask") == 0) {
        q_status.scrollback_save_type = Q_CAPTURE_TYPE_ASK;
    }
}
