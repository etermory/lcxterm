/*
 * console.c
 *
 * lcxterm - Linux Console X-like Terminal
 *
 * Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any
 * warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "common.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include "screen.h"
#include "main.h"
#include "console.h"
#include "keyboard.h"
#include "protocols.h"
#include "states.h"
#include "options.h"
#include "forms.h"

/* Set this to a not-NULL value to enable debug log. */
/* static const char * DLOGNAME = "console"; */
static const char * DLOGNAME = NULL;

/**
 * When true, the console needs a repaint.
 */
Q_BOOL q_screen_dirty = Q_TRUE;

/**
 * Split screen dirty flag.
 */
Q_BOOL q_split_screen_dirty = Q_FALSE;

/**
 * When true, the console needs to use ncurses for keyboard input even though
 * it would normally not want to.
 */
Q_BOOL q_passthru_getch_override = Q_FALSE;

/* The emulation selected before we entered split-screen mode. */
static Q_EMULATION split_screen_emulation;

/* The split-screen keyboard buffer */
static wchar_t split_screen_buffer[254];
static int split_screen_buffer_n;
static int split_screen_x;
static int split_screen_y;

/* MIXED mode doorway keys */
static Q_BOOL doorway_mixed[256];
static Q_BOOL doorway_mixed_pgup;
static Q_BOOL doorway_mixed_pgdn;

#ifndef Q_NO_ZMODEM
/* Zmodem autostart buffer */
static unsigned char zrqinit_buffer[32];
static unsigned int zrqinit_buffer_n;
#endif /* Q_NO_ZMODEM */

#ifndef Q_NO_KERMIT
/* Kermit autostart buffer */
static char kermit_autostart_buffer[32];
static unsigned int kermit_autostart_buffer_n;
#endif /* Q_NO_KERMIT */

/**
 * A flag to indicate a data flood on the console.  We need to not permit
 * download protocol autostarts during a flood.
 */
Q_BOOL q_console_flood = Q_FALSE;

/*
 * Setup MIXED mode doorway
 */
void setup_doorway_handling() {
    char * option;
    char * value;
    int i;
    for (i = 0; i < 256; i++) {
        doorway_mixed[i] = Q_FALSE;
    }
    doorway_mixed_pgup = Q_FALSE;
    doorway_mixed_pgdn = Q_FALSE;

    option = Xstrdup(get_option(Q_OPTION_DOORWAY_MIXED_KEYS),
                     __FILE__, __LINE__);
    value = strtok(option, " ");
    while (value != NULL) {
        if (strlen(value) == 1) {
            doorway_mixed[tolower(value[0])] = Q_TRUE;
            doorway_mixed[toupper(value[0])] = Q_TRUE;
        }
        if (strcasecmp(value, "pgdn") == 0) {
            doorway_mixed_pgdn = Q_TRUE;
        }
        if (strcasecmp(value, "pgup") == 0) {
            doorway_mixed_pgup = Q_TRUE;
        }
        value = strtok(NULL, " ");
    }

    Xfree(option, __FILE__, __LINE__);
}

/**
 * Begin capturing the session to file.
 *
 * @param filename the file to save data to
 */
void start_capture(const char * filename) {
    char * new_filename;
    char time_string[TIME_STRING_LENGTH];
    time_t current_time;
    char notify_message[DIALOG_MESSAGE_SIZE];

    if (q_status.read_only == Q_TRUE) {
        /* Never start capture in read-only mode. */
        return;
    }

    if ((filename != NULL) && (q_status.capture == Q_FALSE)) {
        q_status.capture_file = open_workingdir_file(filename, &new_filename);
        if (q_status.capture_file == NULL) {
            snprintf(notify_message, sizeof(notify_message),
                     _("Error opening file \"%s\" for writing: %s"),
                     new_filename, strerror(errno));
            notify_form(notify_message, 0);
            q_cursor_on();
        } else {
            time(&current_time);

            if (q_status.capture_type == Q_CAPTURE_TYPE_HTML) {
                /*
                 * HTML
                 */
                strftime(time_string, sizeof(time_string),
                         _("Capture Generated %a, %d %b %Y %H:%M:%S %z"),
                         localtime(&current_time));
                fprintf(q_status.capture_file, "<html>\n\n");
                fprintf(q_status.capture_file,
                        "<!-- * - * LCXTerm " Q_VERSION
                        " %s BEGIN * - * --> \n\n", time_string);
#ifdef Q_ENABLE_24BITRGB
                fprintf(q_status.capture_file,
                        "<body bgcolor=\"black\">\n<pre {font-family: 'Courier New', monospace;}><code><font %s>",
                    color_to_html(q_current_color, q_current_color_fg_rgb,
                        q_current_color_bg_rgb));
#else
                fprintf(q_status.capture_file,
                        "<body bgcolor=\"black\">\n<pre {font-family: 'Courier New', monospace;}><code><font %s>",
                    color_to_html(q_current_color));
#endif
            } else {
                strftime(time_string, sizeof(time_string),
                         _("Capture Generated %a, %d %b %Y %H:%M:%S %z"),
                         localtime(&current_time));
                fprintf(q_status.capture_file,
                        "* - * LCXTerm " Q_VERSION " %s BEGIN * - *\n\n",
                        time_string);
            }

            q_status.capture = Q_TRUE;
        }
        if (new_filename != filename) {
            Xfree(new_filename, __FILE__, __LINE__);
        }
    }
}

/**
 * Stop capturing and close the capture file.
 */
void stop_capture() {
    char time_string[TIME_STRING_LENGTH];
    time_t current_time;

    if (q_status.capture == Q_FALSE) {
        return;
    }

    time(&current_time);

    if (q_status.capture_type == Q_CAPTURE_TYPE_HTML) {
        /*
         * HTML
         */
        fprintf(q_status.capture_file, "</code></pre></font>\n</body>\n");
        strftime(time_string, sizeof(time_string),
                 _("Capture Generated %a, %d %b %Y %H:%M:%S %z"),
                 localtime(&current_time));
        fprintf(q_status.capture_file,
                "\n<!-- * - * LCXTerm " Q_VERSION " %s END * - * -->\n",
                time_string);
        fprintf(q_status.capture_file, "\n</html>\n");
    } else {
        strftime(time_string, sizeof(time_string),
                 _("Capture Generated %a, %d %b %Y %H:%M:%S %z"),
                 localtime(&current_time));
        fprintf(q_status.capture_file,
                "\n* - * LCXTerm " Q_VERSION " %s END * - *\n", time_string);
    }

    fclose(q_status.capture_file);
    q_status.capture = Q_FALSE;
}

#ifndef Q_NO_ZMODEM

/**
 * Reset the Zmodem autostart buffer.
 */
static void reset_zmodem_autostart() {
    memset(zrqinit_buffer, 0, sizeof(zrqinit_buffer));
    zrqinit_buffer_n = 0;
}

/**
 * Check if a Zmodem autostart is required.
 *
 * @param from_modem the next byte from the remote side
 * @return true if a Zmodem download should start
 */
static Q_BOOL check_zmodem_autostart(unsigned char from_modem) {

    if (q_status.read_only == Q_TRUE) {
        /* No auto-downloads in read-only mode. */
        return Q_FALSE;
    }

    if (q_console_flood == Q_TRUE) {
        /*
         * No autostart during a console flood
         */
        return Q_FALSE;
    }

    if (q_status.zmodem_autostart == Q_FALSE) {
        return Q_FALSE;
    }

    /*
     * Note: I may need to expand the number of matching strings for this to
     * work.
     */
    if ((ZRQINIT_STRING[zrqinit_buffer_n] == from_modem) ||
        (ZRQINIT_STRING[zrqinit_buffer_n] == '?')) {

        /*
         * Hang onto it, even though we technically don't need it.
         */
        zrqinit_buffer[zrqinit_buffer_n] = from_modem;
        zrqinit_buffer_n++;
        if (zrqinit_buffer_n == strlen(ZRQINIT_STRING)) {
            /*
             * All ZRQINIT characters received, return true.
             */
            return Q_TRUE;
        }

        /*
         * More characters can come.  Return false but don't reset.
         */
        return Q_FALSE;
    }

    /*
     * It didn't match, so reset
     */
    reset_zmodem_autostart();
    return Q_FALSE;
}

#endif /* Q_NO_ZMODEM */

#ifndef Q_NO_KERMIT

/**
 * Reset the Kermit autostart buffer.
 */
static void reset_kermit_autostart() {
    memset(kermit_autostart_buffer, 0, sizeof(kermit_autostart_buffer));
    kermit_autostart_buffer_n = 0;
}

/**
 * Check if a Kermit autostart is required.
 *
 * @param from_modem the next byte from the remote side
 * @return true if a Kermit download should start
 */
static Q_BOOL check_kermit_autostart(unsigned char from_modem) {

    if (q_status.read_only == Q_TRUE) {
        /* No auto-downloads in read-only mode. */
        return Q_FALSE;
    }

    if (q_console_flood == Q_TRUE) {
        /*
         * No autostart during a console flood
         */
        return Q_FALSE;
    }

    if (q_status.kermit_autostart == Q_FALSE) {
        return Q_FALSE;
    }

    /*
     * Note: I may need to expand the number of matching strings for this to
     * work.
     */
    if ((KERMIT_AUTOSTART_STRING[kermit_autostart_buffer_n] == from_modem) ||
        (KERMIT_AUTOSTART_STRING[kermit_autostart_buffer_n] == '?')) {

        /*
         * Hang onto it, even though we technically don't need it.
         */
        kermit_autostart_buffer[kermit_autostart_buffer_n] = from_modem;
        kermit_autostart_buffer_n++;
        if (kermit_autostart_buffer_n == strlen(KERMIT_AUTOSTART_STRING)) {
            /*
             * All KERMIT_AUTOSTART characters received, return true.
             */
            return Q_TRUE;
        }

        /*
         * More characters can come.  Return false but don't reset.
         */
        return Q_FALSE;
    }

    /*
     * It didn't match, so reset
     */
    reset_kermit_autostart();
    return Q_FALSE;
}

#endif /* Q_NO_KERMIT */

/**
 * Enable or disable the Alt-Minus show status line flag.
 */
void set_status_line(Q_BOOL make_visible) {
    if (make_visible == Q_FALSE) {
        /*
         * Increase the scrolling region
         */
        if (q_status.scroll_region_bottom == HEIGHT - STATUS_HEIGHT - 1) {
            q_status.scroll_region_bottom = HEIGHT - 1;
        }
        /*
         * Eliminate the status line
         */
        q_status.status_visible = Q_FALSE;
        STATUS_HEIGHT = 0;
        if (q_status.scrollback_lines >= HEIGHT) {
            q_status.cursor_y++;
        }
        if ((q_status.passthru == Q_TRUE) &&
            ((q_program_state == Q_STATE_CONSOLE) ||
                (q_program_state == Q_STATE_INITIALIZATION))
        ) {
            /*
             * Don't clear when in passthru.
             */
        } else {
            screen_clear();
        }
        q_screen_dirty = Q_TRUE;
    } else {
        q_status.status_visible = Q_TRUE;
        STATUS_HEIGHT = 1;
        /*
         * Decrease the scrolling region
         */
        if (q_status.scroll_region_bottom == HEIGHT - 1) {
            q_status.scroll_region_bottom = HEIGHT - STATUS_HEIGHT - 1;
        }
        if (q_status.cursor_y == HEIGHT - 1) {
            /*
             * On the last line, pull back before
             */
            q_status.cursor_y -= STATUS_HEIGHT;
        } else if (q_status.cursor_y == HEIGHT - 1 - STATUS_HEIGHT) {
            /*
             * On what is about to be the last line
             */
            if (q_status.scrollback_lines >= HEIGHT) {
                /*
                 * Pull back because more lines are present in
                 * the scrollback buffer.
                 */
                q_status.cursor_y--;
            }
        }
        q_screen_dirty = Q_TRUE;
    }

    /*
     * Pass the new height to the remote side.
     */
    send_screen_size();
}

/**
 * Send the sequence representing the beginning of a bracketed paste.
 */
static void bracketed_paste_on() {
    qodem_write(q_child_tty_fd, "\033[200~", 6, Q_TRUE);
}

/**
 * Send the sequence representing the end of a bracketed paste.
 */
static void bracketed_paste_off() {
    qodem_write(q_child_tty_fd, "\033[201~", 6, Q_TRUE);
}

/**
 * Keyboard handler for the normal console.
 *
 * @param keystroke the keystroke from the user.
 * @param flags KEY_FLAG_ALT, KEY_FLAG_CTRL, etc.  See input.h.
 */
void console_keyboard_handler(int keystroke, int flags) {
    char * filename;
    char notify_message[DIALOG_MESSAGE_SIZE];
    char command_line[COMMAND_LINE_SIZE];
    int i;
    int new_keystroke;

    DLOG(("console_keyboard_handler() Keystroke '%c' %d 0x%x %o flags %d\n",
            keystroke, keystroke, keystroke, keystroke, flags));
    DLOG(("                           doorway = %d\n", q_status.doorway_mode));

    /*
     * Passthru: behave like pure doorway mode.
     */
    if (q_status.passthru == Q_TRUE) {
        post_keystroke(keystroke, flags);
        return;
    }

    /*
     * Bracketed paste mode.
     */
    if (keystroke == Q_KEY_BRACKET_ON) {
        if (q_status.bracketed_paste_mode == Q_TRUE) {
            bracketed_paste_on();
        }
        return;
    }
    if (keystroke == Q_KEY_BRACKET_OFF) {
        if (q_status.bracketed_paste_mode == Q_TRUE) {
            bracketed_paste_off();
        }
        return;
    }

    if (q_status.doorway_mode == Q_DOORWAY_MODE_FULL) {
        if ((keystroke == '=') && (flags & KEY_FLAG_ALT)) {
            /*
             * Alt-= Doorway mode
             */
            q_status.doorway_mode = Q_DOORWAY_MODE_MIXED;
            notify_form(_("Doorway MIXED"), 1.5);
            q_cursor_on();
        } else {
            /*
             * Pass keystroke
             */
            post_keystroke(keystroke, flags);
        }
        return;
    }

    if (q_status.doorway_mode == Q_DOORWAY_MODE_MIXED) {
        if ((keystroke == '=') && (flags & KEY_FLAG_ALT)) {
            /*
             * Alt-= Doorway mode
             */
            q_status.doorway_mode = Q_DOORWAY_MODE_OFF;
            notify_form(_("Doorway OFF"), 1.5);
            q_cursor_on();
            return;
        } else {
            if (q_key_code_yes(keystroke)) {
                /*
                 * See if this is PgUp or PgDn
                 */
                if ((keystroke == Q_KEY_NPAGE) && (flags == 0) &&
                    (doorway_mixed_pgdn == Q_TRUE)) {
                    /*
                     * Raw PgDn, pass it on
                     */
                    post_keystroke(keystroke, flags);
                    return;
                }
                if ((keystroke == Q_KEY_PPAGE) && (flags == 0) &&
                    (doorway_mixed_pgup == Q_TRUE)) {
                    /*
                     * Raw PgUp, pass it on
                     */
                    post_keystroke(keystroke, flags);
                    return;
                }
                if ((keystroke == Q_KEY_PPAGE) && (flags == KEY_FLAG_SHIFT)) {
                    /*
                     * Shift-PgUp, switch to scrollback, and pass this PgUp
                     * to it to go up a screen.
                     */
                    switch_state(Q_STATE_SCROLLBACK);
                    scrollback_keyboard_handler(keystroke, flags);
                    return;
                }
                if ((keystroke == Q_KEY_NPAGE) || (keystroke == Q_KEY_PPAGE)) {
                    /*
                     * Modified PgUp/PgDn, handle it below
                     */
                } else {
                    /*
                     * Some other function key, pass it on
                     */
                    post_keystroke(keystroke, flags);
                    return;
                }
            } else if ((doorway_mixed[keystroke] == Q_TRUE) ||
                       (keystroke == 0x03) || (keystroke == 0x00)
                ) {
                /*
                 * This keystroke is supposed to be handled, so honor it.  We
                 * pass Ctrl-C and Ctrl-Space to the block below so that
                 * MIXED mode has the same easy exit when not connected.
                 */
            } else {
                /*
                 * Pass keystroke
                 */
                post_keystroke(keystroke, flags);
                return;
            }
        }
    }

    switch (keystroke) {

    case '7':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-7 Status line info
             */
            if (q_status.status_line_info == Q_TRUE) {
                q_status.status_line_info = Q_FALSE;
            } else {
                q_status.status_line_info = Q_TRUE;
            }
            return;
        }
        break;

    case '8':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-8 8th bit strip
             */
            if (q_status.strip_8th_bit == Q_TRUE) {
                q_status.strip_8th_bit = Q_FALSE;
                notify_form(_("Strip 8th OFF"), 1.5);
            } else {
                q_status.strip_8th_bit = Q_TRUE;
                notify_form(_("Strip 8th ON"), 1.5);
            }
            q_cursor_on();
            return;
        }
        break;

    case '-':
        /*
         * Alt-- Status Lines
         */
        if (flags & KEY_FLAG_ALT) {
            if (q_status.status_visible == Q_TRUE) {
                set_status_line(Q_FALSE);
            } else {
                set_status_line(Q_TRUE);
            }
            return;
        }
        break;

    case '+':
        /*
         * Alt-+ CR/CRLF
         */
        if (flags & KEY_FLAG_ALT) {
            if (q_status.line_feed_on_cr == Q_TRUE) {
                q_status.line_feed_on_cr = Q_FALSE;
                notify_form(_("Add LF OFF"), 1.5);
            } else {
                q_status.line_feed_on_cr = Q_TRUE;
                notify_form(_("Add LF ON"), 1.5);
            }
            q_cursor_on();
            return;
        }
        break;

    case '=':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-= Doorway mode
             */
            q_status.doorway_mode = Q_DOORWAY_MODE_FULL;
            notify_form(_("Doorway FULL"), 1.5);
            q_cursor_on();
            return;
        }
        break;

    case 'B':
    case 'b':
        /*
         * Alt-B Beeps and bells
         */
        if (flags & KEY_FLAG_ALT) {
            if (q_status.beeps == Q_TRUE) {
                q_status.beeps = Q_FALSE;
                notify_form(_("Beeps & Bells OFF"), 1.5);
            } else {
                q_status.beeps = Q_TRUE;
                notify_form(_("Beeps & Bells ON"), 1.5);
            }
            q_cursor_on();
            return;
        }
        break;

    case 'C':
    case 'c':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-C Clear screen
             */
            cursor_formfeed();

            /*
             * Switch back to default color
             */
            q_current_color =
                Q_A_NORMAL | scrollback_full_attr(Q_COLOR_CONSOLE_TEXT);
#ifdef Q_ENABLE_24BITRGB
            q_current_color_fg_rgb = -1;
            q_current_color_bg_rgb = -1;
#endif
            q_screen_dirty = Q_TRUE;
            return;
        }
        break;

    case 'E':
    case 'e':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-E Half/full duplex
             */
            if (q_status.full_duplex == Q_TRUE) {
                q_status.full_duplex = Q_FALSE;
                notify_form(_("Half Duplex"), 1.5);
            } else {
                q_status.full_duplex = Q_TRUE;
                notify_form(_("Full Duplex"), 1.5);
            }
            q_cursor_on();
            return;
        }
        break;

    case 'G':
    case 'g':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-G Term Emulation
             */
            if (q_status.split_screen == Q_FALSE) {
                switch_state(Q_STATE_EMULATION_MENU);
            }
            return;
        }
        break;

    case 'H':
    case 'h':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-H Kill Shell
             */
            if (q_status.online == Q_TRUE) {
                new_keystroke = q_tolower(
                    notify_prompt_form(_("Kill"),
                        _("Kill Shell? [Y/n] "),
                        _(" Y-Kill Shell   N-Exit "),
                        Q_TRUE, 0.0, "YyNn\r"));
                if ((new_keystroke == 'y') || (new_keystroke == Q_KEY_ENTER)) {
                    kill_shell();
                }
            }
            return;
        }
        break;

#ifndef Q_NO_KEYMACROS
    case 'J':
    case 'j':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-J Function Keys
             */
            switch_state(Q_STATE_FUNCTION_KEY_EDITOR);
            return;
        }
        break;
#endif

    case 'N':
    case 'n':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-N Configuration
             */
            screen_clear();
            screen_put_str_yx(0, 0, _("Spawning editor...\n\n"), Q_A_NORMAL, 0);
            screen_flush();
            sprintf(command_line, "%s %s", get_option(Q_OPTION_EDITOR),
                    get_options_filename());
            spawn_terminal(command_line);

            /*
             * Reload options from file
             */
            load_options();
            load_colors();
            q_current_color = Q_A_NORMAL |
                scrollback_full_attr(Q_COLOR_CONSOLE_TEXT);

            /*
             * Explicitly check for the mouse reporting flag.  We need to do
             * this here and not in load_options() because the first
             * invocation to load_options() occurs before screen_setup().
             */
            if ((q_status.xterm_mouse_reporting == Q_TRUE) &&
                ((q_status.emulation == Q_EMUL_XTERM) ||
                 (q_status.emulation == Q_EMUL_XTERM_UTF8))
            ) {
                /*
                 * xterm emulations: listen for the mouse.
                 */
                enable_mouse_listener();
            } else {
                /*
                 * Non-xterm or mouse disabled, do not listen for the mouse.
                 */
                disable_mouse_listener();
            }

            return;
        }
        break;

    case 'P':
    case 'p':
        if ((flags & KEY_FLAG_ALT) && (q_status.read_only == Q_FALSE)) {
            /*
             * Alt-P Capture File
             */
            if (q_status.capture == Q_FALSE) {
                reset_capture_type();
                if (q_status.capture_type == Q_CAPTURE_TYPE_ASK) {
                    q_status.capture_type = ask_capture_type();
                    q_screen_dirty = Q_TRUE;
                    console_refresh(Q_FALSE);
                }
                if (q_status.capture_type != Q_CAPTURE_TYPE_ASK) {
                    filename = save_form(_("Capture Filename"),
                                         get_option(Q_OPTION_CAPTURE_FILE),
                                         Q_FALSE, Q_FALSE);
                    if (filename != NULL) {
                        start_capture(filename);
                        Xfree(filename, __FILE__, __LINE__);
                    }
                }
            } else {
                stop_capture();
                notify_form(_("Capture OFF"), 1.5);
                q_cursor_on();
            }
            return;
        }
        break;

    case 'R':
    case 'r':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-R OS shell
             */
            screen_clear();
            screen_put_str_yx(0, 0, _("Spawning system shell...\n\n"),
                              Q_A_NORMAL, 0);
            screen_flush();
            spawn_terminal(get_option(Q_OPTION_SHELL));
            return;
        }
        break;

    case 'S':
    case 's':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-S Split Screen
             */
            if (q_status.split_screen == Q_FALSE) {
                q_status.split_screen = Q_TRUE;
                split_screen_emulation = q_status.emulation;
                memset(split_screen_buffer, 0, sizeof(split_screen_buffer));
                split_screen_buffer_n = 0;
                split_screen_x = 0;
                split_screen_y = HEIGHT - 1 - STATUS_HEIGHT - 4;
                q_split_screen_dirty = Q_TRUE;
            } else {
                q_status.split_screen = Q_FALSE;
                q_status.emulation = split_screen_emulation;
                split_screen_x = 0;
                split_screen_y = 0;
            }
            cursor_formfeed();
            reset_emulation();
            /*
             * To avoid the "disappearing line" let's move the "real" cursor
             * to (6,0) so it'll be the top corner of the split-screen
             * screen.
             */
            if (q_status.split_screen == Q_TRUE) {
                cursor_position(6, 0);
            }
            q_screen_dirty = Q_TRUE;
            return;
        }
        break;

    case 'T':
    case 't':
        if ((flags & KEY_FLAG_ALT) && (q_status.read_only == Q_FALSE)) {
            /*
             * Alt-T Screen dump
             */
            reset_screen_dump_type();
            if (q_status.screen_dump_type == Q_CAPTURE_TYPE_ASK) {
                q_status.screen_dump_type = ask_save_type();
                q_screen_dirty = Q_TRUE;
                console_refresh(Q_FALSE);
            }
            if (q_status.screen_dump_type != Q_CAPTURE_TYPE_ASK) {
                filename =
                    save_form(_("Screen Dump Filename"), _("screen_dump.txt"),
                              Q_FALSE, Q_FALSE);
                if (filename != NULL) {
                    if (screen_dump(filename) == Q_FALSE) {
                        snprintf(notify_message, sizeof(notify_message),
                                 _("Error saving to file \"%s\""), filename);
                        notify_form(notify_message, 0);
                        q_cursor_on();
                    }
                    Xfree(filename, __FILE__, __LINE__);
                }
            }
            return;
        }
        break;

    case 'U':
    case 'u':
        /*
         * Alt-U Scrollback
         */
        if (flags & KEY_FLAG_ALT) {
            if (q_status.scrollback_enabled == Q_TRUE) {
                q_status.scrollback_enabled = Q_FALSE;
                notify_form(_("Scrollback OFF"), 1.5);
            } else {
                q_status.scrollback_enabled = Q_TRUE;
                notify_form(_("Scrollback ON"), 1.5);
            }
            q_cursor_on();
            return;
        }
        break;

    case 'V':
    case 'v':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-V View a File
             */
            filename =
                save_form(_("View File"), get_option(Q_OPTION_WORKING_DIR),
                          Q_FALSE, Q_FALSE);
            if (filename != NULL) {
                screen_clear();
                screen_put_str_yx(0, 0, _("Spawning editor...\n\n"), Q_A_NORMAL,
                                  0);
                screen_flush();
                sprintf(command_line, "%s %s", get_option(Q_OPTION_EDITOR),
                        filename);
                spawn_terminal(command_line);
                Xfree(filename, __FILE__, __LINE__);
            }
            return;
        }
        break;

    case 'W':
    case 'w':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-W View Directory
             */
            filename =
                save_form(_("List Directory"), get_option(Q_OPTION_WORKING_DIR),
                          Q_TRUE, Q_FALSE);
            if (filename != NULL) {
                q_cursor_off();
                view_directory(filename, "*");
                q_cursor_on();
                q_screen_dirty = Q_TRUE;
                Xfree(filename, __FILE__, __LINE__);
            }
            return;
        }
        break;

    case 'Z':
    case 'z':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-Z Menu
             */
            switch_state(Q_STATE_CONSOLE_MENU);
            return;
        }
        break;

    default:
        break;

    }

    switch (keystroke) {

    case Q_KEY_PPAGE:
        if (flags == KEY_FLAG_SHIFT) {
            /*
             * Shift-PgUp, switch to scrollback, and pass this PgUp to it to
             * go up a screen.
             */
            switch_state(Q_STATE_SCROLLBACK);
            scrollback_keyboard_handler(keystroke, flags);
            return;
        }
#ifndef Q_NO_PROTOCOLS
        if ((flags & KEY_FLAG_UNICODE) == 0) {
            /*
             * PgUp Upload
             */
            switch_state(Q_STATE_UPLOAD_MENU);
        }
#endif
        return;

    case Q_KEY_NPAGE:
#ifndef Q_NO_PROTOCOLS
        if (((flags & KEY_FLAG_UNICODE) == 0) &&
            (q_status.read_only == Q_FALSE)
        ) {
            /*
             * PgDn Download
             */
            switch_state(Q_STATE_DOWNLOAD_MENU);
        }
#endif
        return;

    case '/':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-/ Scrollback
             */
            switch_state(Q_STATE_SCROLLBACK);
            return;
        }
        break;

    case '\\':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-\ Alt Code key
             */
            if ((q_status.emulation == Q_EMUL_LINUX_UTF8) ||
                (q_status.emulation == Q_EMUL_XTERM_UTF8)) {
                new_keystroke = alt_code_key(Q_TRUE);
            } else {
                new_keystroke = alt_code_key(Q_FALSE);
            }
            if (new_keystroke != -1) {
                keystroke = new_keystroke;
                flags &= ~KEY_FLAG_ALT;
                if ((q_status.emulation == Q_EMUL_LINUX_UTF8) ||
                    (q_status.emulation == Q_EMUL_XTERM_UTF8)) {
                    flags |= KEY_FLAG_UNICODE;
                } else {
                    keystroke = codepage_map_char(keystroke);
                }
            } else {
                return;
            }
        }
        break;

    case ';':
        if (flags & KEY_FLAG_ALT) {
            /*
             * Alt-; Codepage
             */
            if (q_status.split_screen == Q_FALSE) {
                switch_state(Q_STATE_CODEPAGE);
            }
            return;
        }
        break;

    default:
        break;
    }

    /*
     * Pass keystroke
     */
    if (q_status.split_screen == Q_FALSE) {
        post_keystroke(keystroke, flags);
        return;
    }

    /*
     * Split screen: save character UNLESS it's an ENTER
     */
    if (keystroke == Q_KEY_ENTER) {
        /*
         * Transmit entire buffer
         */
        for (i = 0; i < split_screen_buffer_n; i++) {
            if ((split_screen_buffer[i] == '^') &&
                (i + 1 < split_screen_buffer_n) &&
                (split_screen_buffer[i + 1] >= 'A') &&
                (split_screen_buffer[i + 1] <= '_')
            ) {
                post_keystroke(split_screen_buffer[i + 1] - 0x40,
                    KEY_FLAG_CTRL);
                i++;
                continue;
            }
            if (split_screen_buffer[i] > 0xFF) {
                post_keystroke(split_screen_buffer[i], KEY_FLAG_UNICODE);
            } else {
                post_keystroke(split_screen_buffer[i], 0);
            }
        }
        memset(split_screen_buffer, 0, sizeof(split_screen_buffer));
        split_screen_buffer_n = 0;
        q_screen_dirty = Q_TRUE;
        q_split_screen_dirty = Q_TRUE;
        split_screen_x = 0;
        split_screen_y = HEIGHT - 1 - STATUS_HEIGHT - 4;
        return;
    }

    /*
     * Split screen: BACKSPACE
     */
    if ((keystroke == Q_KEY_BACKSPACE) ||
        (keystroke == Q_KEY_DC)
    ) {
        if (split_screen_buffer_n > 0) {

            split_screen_x--;
            screen_put_color_char_yx(split_screen_y, split_screen_x, ' ',
                                     Q_COLOR_CONSOLE_TEXT);
            screen_move_yx(split_screen_y, split_screen_x);
            if (split_screen_x < 0) {
                split_screen_y--;
                split_screen_x = 0;
            }
            q_split_screen_dirty = Q_TRUE;
            split_screen_buffer[split_screen_buffer_n] = '\0';
            split_screen_buffer_n--;
        }
        return;
    }

    if (split_screen_buffer_n < sizeof(split_screen_buffer)) {
        /*
         * Append keystroke to buffer.
         */
        if (!q_key_code_yes(keystroke) || ((flags & KEY_FLAG_UNICODE) != 0)) {
            if (((flags & KEY_FLAG_UNICODE) == 0) && (keystroke < 0x20)) {
                if (split_screen_buffer_n < sizeof(split_screen_buffer) - 2) {
                    /*
                     * Control character, put it in carat notation.
                     */
                    split_screen_buffer[split_screen_buffer_n] = '^';
                    split_screen_buffer_n++;
                    split_screen_buffer[split_screen_buffer_n] = keystroke + 0x40;
                    split_screen_buffer_n++;
                    split_screen_x++;
                    if (split_screen_x == WIDTH) {
                        split_screen_x = 0;
                        split_screen_y++;
                    }
                    split_screen_x++;
                    if (split_screen_x == WIDTH) {
                        split_screen_x = 0;
                        split_screen_y++;
                    }
                    q_split_screen_dirty = Q_TRUE;
                    return;
                }
            }

            /*
             * Normal character (or Unicode), save it in the buffer.
             */
            split_screen_buffer[split_screen_buffer_n] = (wchar_t) keystroke;
            split_screen_buffer_n++;
            screen_put_color_char((wchar_t) keystroke, Q_COLOR_CONSOLE_TEXT);
            split_screen_x++;
            if (split_screen_x == WIDTH) {
                split_screen_x = 0;
                split_screen_y++;
            }
            q_split_screen_dirty = Q_TRUE;
        }
        return;
    }
}

/**
 * Process raw bytes from the remote side through the emulation layer,
 * handling zmodem/kermit autostart, translation tables, etc.
 *
 * @param buffer the bytes from the remote side
 * @param n the number of bytes in buffer
 * @param remaining the number of un-processed bytes that should be sent
 * through a future invocation of console_process_incoming_data
 */
void console_process_incoming_data(unsigned char * buffer, const int n,
                                   int * remaining) {
    int i;
    wchar_t emulated_char;
    Q_EMULATION_STATUS emulation_rc;

    for (i = 0; i < n; i++) {

        /*
         * Capture
         */
        if (q_status.capture == Q_TRUE) {
            if (q_status.capture_type == Q_CAPTURE_TYPE_RAW) {
                /*
                 * Raw
                 */
                fprintf(q_status.capture_file, "%c", (buffer[i] & 0xFF));
                if (q_status.capture_flush_time < time(NULL)) {
                    fflush(q_status.capture_file);
                    q_status.capture_flush_time = time(NULL);
                }
            }
        }

        /*
         * Strip 8th bit processing
         */
        if (q_status.strip_8th_bit == Q_TRUE) {
            buffer[i] &= 0x7F;
        }

        /*
         * Only do Zmodem and Kermit autostart when in actual console mode.
         */
        if (q_program_state == Q_STATE_CONSOLE) {

#ifndef Q_NO_ZMODEM

            /*
             * Check for Zmodem autostart
             */
            if (check_zmodem_autostart(buffer[i]) == Q_TRUE) {
                if (q_download_location == NULL) {
                    if (q_status.passthru == Q_TRUE) {
                        screen_setup();
                        initialize_keyboard();
                        q_passthru_getch_override = Q_TRUE;
                    }
                    q_download_location =
                        save_form(_("Download Directory"),
                                  get_option(Q_OPTION_DOWNLOAD_DIR), Q_TRUE,
                                  Q_FALSE);
                }
                q_passthru_getch_override = Q_FALSE;

                if (q_download_location != NULL) {
                    q_transfer_stats.protocol = Q_PROTOCOL_ZMODEM;
                    switch_state(Q_STATE_DOWNLOAD);
                    start_file_transfer();
                } else {
                    if (q_status.passthru == Q_TRUE) {
                        screen_teardown();
                    }
                }

                /*
                 * Reset check for Zmodem autostart
                 */
                reset_zmodem_autostart();

                /*
                 * Get out of here
                 */
                return;
            }

#endif /* Q_NO_ZMODEM */

#ifndef Q_NO_KERMIT

            /*
             * Check for Kermit autostart
             */
            if (check_kermit_autostart(buffer[i]) == Q_TRUE) {
                if (q_download_location == NULL) {
                    if (q_status.passthru == Q_TRUE) {
                        screen_setup();
                        initialize_keyboard();
                        q_passthru_getch_override = Q_TRUE;
                    }
                    q_download_location =
                        save_form(_("Download Directory"),
                                  get_option(Q_OPTION_DOWNLOAD_DIR), Q_TRUE,
                                  Q_FALSE);
                }
                q_passthru_getch_override = Q_FALSE;

                if (q_download_location != NULL) {
                    q_transfer_stats.protocol = Q_PROTOCOL_KERMIT;
                    switch_state(Q_STATE_DOWNLOAD);
                    start_file_transfer();
                } else {
                    if (q_status.passthru == Q_TRUE) {
                        screen_teardown();
                    }
                }

                /*
                 * Reset check for Kermit autostart
                 */
                reset_kermit_autostart();

                /*
                 * Get out of here
                 */
                return;
            }

#endif /* Q_NO_KERMIT */

        } /* if (q_status.state == Q_STATE_CONSOLE) */

        /*
         * Passthru: emit the byte directly to STDOUT, in addition to running
         * through the emulator below.
         */
        if (q_status.passthru == Q_TRUE) {
            write(STDOUT_FILENO, &buffer[i], 1);
        }

        /*
         * Normal character -- pass it through emulator
         */
        emulation_rc = terminal_emulator(buffer[i], &emulated_char);
        *remaining -= 1;

        DLOG(("terminal_emulator() (outside) RC %d char '%lc' 0x%x\n",
                emulation_rc, emulated_char, emulated_char));

        for (;;) {

            if (emulation_rc == Q_EMUL_FSM_ONE_CHAR) {

                /*
                 * Print this character
                 */
                print_character(emulated_char);

                /*
                 * We grabbed the one character, get out
                 */
                break;
            } else if (emulation_rc == Q_EMUL_FSM_NO_CHAR_YET) {

                /*
                 * No more characters, break out
                 */
                break;
            } else {
                /*
                 * Q_EMUL_FSM_MANY_CHARS
                 */

                /*
                 * Print this character
                 */
                print_character(emulated_char);

                /*
                 * ...and continue pulling more characters
                 */
                emulation_rc = terminal_emulator(-1, &emulated_char);

                DLOG(("terminal_emulator() (inside) RC %d char '%lc' 0x%x\n",
                        emulation_rc, emulated_char, emulated_char));

            }

        } /* for (;;) */

    } /* for (i = 0; i < n; i++) */

    q_screen_dirty = Q_TRUE;
    if (q_status.split_screen == Q_TRUE) {
        q_split_screen_dirty = Q_TRUE;
    }
}

/**
 * Draw screen for the normal console.
 *
 * @param status_line if true, draw the status line with online/offline,
 * codepage, etc.
 */
void console_refresh(Q_BOOL status_line) {
    static Q_BOOL first = Q_TRUE;
    char * online_string;
    char time_string[32];
    time_t current_time;
    char dec_leds_string[6];

    if (first == Q_TRUE) {
        first = Q_FALSE;
        /*
         * Initial line
         */
        new_scrollback_line();
        q_scrollback_current = q_scrollback_last;
        q_scrollback_position = q_scrollback_current;
        q_status.cursor_y = 0;
        q_status.cursor_x = 0;
    }

    /*
     * Passthru: setup scrollback, but bail out without drawing anything.
     */
    if ((q_status.passthru == Q_TRUE) &&
        ((q_program_state == Q_STATE_CONSOLE) ||
            (q_program_state == Q_STATE_INITIALIZATION))
    ) {
        return;
    }

    /*
     * Render scrollback
     */
    if (q_screen_dirty == Q_TRUE) {

        if (q_status.split_screen == Q_TRUE) {
            int i;

            /*
             * Steal 6 lines from the scrollback buffer display: 5 lines of
             * split-screen area + 1 line for the split-screen status bar.
             */
            render_scrollback(6);

            /*
             * Clear the bottom lines
             */
            /*
             * Start 4 lines above the bottom.
             */
            for (i = (HEIGHT - STATUS_HEIGHT - 1 - 4);
                 i < (HEIGHT - STATUS_HEIGHT); i++) {
                screen_put_color_hline_yx(i, 0, ' ', WIDTH,
                                          Q_COLOR_CONSOLE_TEXT);
            }
        } else {
            render_scrollback(0);
        }
        q_screen_dirty = Q_FALSE;
    }

    /*
     * Render split screen
     */
    if (q_split_screen_dirty == Q_TRUE) {
        char * title;
        int left_stop;
        int i, row;

        /*
         * Put the Split Screen line
         */
        title = _(" Split Screen ");
        left_stop = WIDTH - strlen(title);
        if (left_stop < 0) {
            left_stop = 0;
        } else {
            left_stop /= 2;
        }
        screen_put_color_hline_yx(HEIGHT - 1 - STATUS_HEIGHT - 5, 0,
                                  cp437_chars[DOUBLE_BAR], WIDTH,
                                  Q_COLOR_WINDOW_BORDER);
        screen_put_color_char_yx(HEIGHT - 1 - STATUS_HEIGHT - 5, 3, '[',
                                 Q_COLOR_WINDOW_BORDER);
        screen_put_color_printf(Q_COLOR_MENU_COMMAND,
                                _(" Keystrokes Queued: %d "),
                                split_screen_buffer_n);
        screen_put_color_char(']', Q_COLOR_WINDOW_BORDER);

        screen_put_color_char_yx(HEIGHT - 1 - STATUS_HEIGHT - 5, left_stop - 1,
                                 '[', Q_COLOR_WINDOW_BORDER);
        screen_put_color_str(title, Q_COLOR_MENU_TEXT);
        screen_put_color_char(']', Q_COLOR_WINDOW_BORDER);

        /*
         * Render the characters in the keyboard buffer
         */
        row = HEIGHT - 1 - STATUS_HEIGHT - 4;
        screen_move_yx(row, 0);
        for (i = 0; i < split_screen_buffer_n; i++) {
            if ((i > 0) && ((i % WIDTH) == 0)) {
                row++;
                screen_move_yx(row, 0);
            }
            screen_put_color_char(split_screen_buffer[i], Q_COLOR_CONSOLE_TEXT);
        }
        q_split_screen_dirty = Q_FALSE;
    }

    /*
     * Render status line
     */
    if ((q_status.status_visible == Q_TRUE) &&
        (status_line == Q_TRUE)
#ifndef Q_NO_PROTOCOLS
        &&
        (q_program_state != Q_STATE_DOWNLOAD) &&
        (q_program_state != Q_STATE_UPLOAD) &&
        (q_program_state != Q_STATE_UPLOAD_BATCH)
#endif
    ) {

        /*
         * Put up the status line
         */
        screen_put_color_hline_yx(HEIGHT - 1, 0, ' ', WIDTH, Q_COLOR_STATUS);

        if (q_status.status_line_info == Q_TRUE) {

            /*
             * Format on right side:
             *
             * < > <Offline | Online > < > <Codepage> < > <Address> < >
             * <VT100 LEDS> < > <Modem lines>
             *
             * Format on left side:
             *
             * YYYY-MM-DD HH:mm:ss <3 spaces>
             */

            if ((q_status.online == Q_TRUE) &&
                (q_status.doorway_mode == Q_DOORWAY_MODE_OFF)) {
                online_string = _("Online");
            } else if ((q_status.online == Q_FALSE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_OFF)) {
                online_string = _("Offline");
            } else if ((q_status.online == Q_TRUE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_FULL)) {
                online_string = _("DOORWAY");
            } else if ((q_status.online == Q_FALSE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_FULL)) {
                online_string = _("doorway");
            } else if ((q_status.online == Q_TRUE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_MIXED)) {
                online_string = _("MIXED");
            } else if ((q_status.online == Q_FALSE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_MIXED)) {
                online_string = _("mixed");
            } else {
                /*
                 * BUG
                 */
                abort();
            }

            screen_put_color_str_yx(HEIGHT - 1, 1, online_string,
                                    Q_COLOR_STATUS);

            screen_put_color_str_yx(HEIGHT - 1, 9,
                                    codepage_string(q_status.codepage),
                                    Q_COLOR_STATUS);

            /*
             * DEC leds
             */
            switch (q_status.emulation) {
            case Q_EMUL_VT100:
            case Q_EMUL_VT102:
            case Q_EMUL_VT220:
            case Q_EMUL_LINUX:
            case Q_EMUL_XTERM:
            case Q_EMUL_LINUX_UTF8:
            case Q_EMUL_XTERM_UTF8:
                sprintf(dec_leds_string, "L%c%c%c%c",
                        (q_status.led_1 == Q_TRUE ? '1' : ' '),
                        (q_status.led_2 == Q_TRUE ? '2' : ' '),
                        (q_status.led_3 == Q_TRUE ? '3' : ' '),
                        (q_status.led_4 == Q_TRUE ? '4' : ' '));
                break;
            default:
                memset(dec_leds_string, 0, sizeof(dec_leds_string));
                break;
            }
            screen_put_color_str_yx(HEIGHT - 1, 51, dec_leds_string,
                                    Q_COLOR_STATUS);

            time(&current_time);
            strftime(time_string, sizeof(time_string),
                     "%Y-%m-%d %H:%M:%S", localtime(&current_time));
            screen_put_color_str_yx(HEIGHT - 1, WIDTH - strlen(time_string) - 3,
                                    time_string, Q_COLOR_STATUS);

        } else {

            /*
             * Format on right side:
             *
             * < > <EMULATION padded to 7> < > <Offline | Online >
             * <baud padded to 6> < > <8N1> <3 spaces>
             * "[ALT-Z]-Menu" <8 spaces> "LF X " C_CR " CP LG " 0x18
             *
             * Format on left side:
             *
             * HH:mm:ss <3 spaces>
             */

            if ((q_status.online == Q_TRUE) &&
                (q_status.doorway_mode == Q_DOORWAY_MODE_OFF)) {
                online_string = _("Online");
            } else if ((q_status.online == Q_FALSE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_OFF)) {
                online_string = _("Offline");
            } else if ((q_status.online == Q_TRUE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_FULL)) {
                online_string = _("DOORWAY");
            } else if ((q_status.online == Q_FALSE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_FULL)) {
                online_string = _("doorway");
            } else if ((q_status.online == Q_TRUE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_MIXED)) {
                online_string = _("MIXED");
            } else if ((q_status.online == Q_FALSE) &&
                       (q_status.doorway_mode == Q_DOORWAY_MODE_MIXED)) {
                online_string = _("mixed");
            } else {
                /*
                 * BUG
                 */
                abort();
            }

            if (q_status.online == Q_TRUE) {
                /*
                 * time_string needs to be hours/minutes/seconds ONLINE
                 */
                int hours, minutes, seconds;
                time_t online_time;
                time(&current_time);
                online_time = (time_t) difftime(current_time,
                                                q_status.connect_time);
                hours   = (int)  (online_time / 3600);
                minutes = (int) ((online_time % 3600) / 60);
                seconds = (int)  (online_time % 60);
                snprintf(time_string, sizeof(time_string), "%02u:%02u:%02u",
                         hours, minutes, seconds);
            } else {
                time(&current_time);
                strftime(time_string, sizeof(time_string), "%H:%M:%S",
                         localtime(&current_time));
            }

            screen_put_color_str_yx(HEIGHT - 1, 1,
                                    emulation_string(q_status.emulation),
                                    Q_COLOR_STATUS);
            screen_put_color_str_yx(HEIGHT - 1, 9, online_string,
                                    Q_COLOR_STATUS);
            screen_put_color_str_yx(HEIGHT - 1, 17, _("[ALT-Z]-Menu"),
                                    Q_COLOR_STATUS);
            if (q_status.online == Q_TRUE) {
                screen_put_color_str_yx(HEIGHT - 1, 30, _("LOCAL"),
                                        Q_COLOR_STATUS);
            }

            if (q_status.full_duplex == Q_TRUE) {
                screen_put_color_str_yx(HEIGHT - 1, 45, _("FDX"),
                                        Q_COLOR_STATUS);
            } else {
                screen_put_color_str_yx(HEIGHT - 1, 45, _("HDX"),
                                        Q_COLOR_STATUS);
            }
            if (q_status.strip_8th_bit == Q_TRUE) {
                screen_put_color_str_yx(HEIGHT - 1, 49, "7", Q_COLOR_STATUS);
            } else {
                screen_put_color_str_yx(HEIGHT - 1, 49, "8", Q_COLOR_STATUS);
            }

            if (q_status.line_feed_on_cr == Q_FALSE) {
                screen_put_color_str_yx(HEIGHT - 1, 51, _("LF"),
                                        Q_COLOR_STATUS_DISABLED);
            } else {
                screen_put_color_str_yx(HEIGHT - 1, 51, _("LF"),
                                        Q_COLOR_STATUS);
            }

            /*
             * BEEPS/BELLS
             */
            if (q_status.beeps == Q_TRUE) {
                screen_put_color_char_yx(HEIGHT - 1, 54, cp437_chars[0x0D],
                                         Q_COLOR_STATUS);
            } else {
                screen_put_color_char_yx(HEIGHT - 1, 54, cp437_chars[0x0D],
                                         Q_COLOR_STATUS_DISABLED);
            }

            if (q_status.capture == Q_FALSE) {
                screen_put_color_str_yx(HEIGHT - 1, 58, _("CP"),
                                        Q_COLOR_STATUS_DISABLED);
            } else {
                screen_put_color_str_yx(HEIGHT - 1, 58, _("CP"),
                                        Q_COLOR_STATUS);
            }

            if (q_status.scrollback_enabled == Q_FALSE) {
                screen_put_color_char_yx(HEIGHT - 1, 64, cp437_chars[UPARROW],
                                         Q_COLOR_STATUS_DISABLED);
            } else {
                screen_put_color_char_yx(HEIGHT - 1, 64, cp437_chars[UPARROW],
                                         Q_COLOR_STATUS);
            }
            screen_put_color_str_yx(HEIGHT - 1, WIDTH - strlen(time_string) - 3,
                                    time_string, Q_COLOR_STATUS);

        } /* if (q_status.status_line_info == Q_TRUE) */

    } /* if (q_status.status_visible == Q_TRUE) */

    /*
     * Position cursor
     */
    if (q_status.split_screen == Q_TRUE) {
        screen_move_yx(split_screen_y, split_screen_x);
    } else {
        if (q_scrollback_current->double_width == Q_TRUE) {
            if (has_true_doublewidth() == Q_FALSE) {
                screen_move_yx(q_status.cursor_y, (2 * q_status.cursor_x));
            } else {
                screen_move_yx(q_status.cursor_y, q_status.cursor_x);
            }
        } else {
            screen_move_yx(q_status.cursor_y, q_status.cursor_x);
        }
    }

    switch (q_program_state) {
    case Q_STATE_CONSOLE:
        screen_flush();
        break;
    case Q_STATE_CONSOLE_MENU:
    case Q_STATE_SCROLLBACK:
#ifndef Q_NO_PROTOCOLS
    case Q_STATE_DOWNLOAD_MENU:
    case Q_STATE_UPLOAD_MENU:
    case Q_STATE_DOWNLOAD_PATHDIALOG:
    case Q_STATE_UPLOAD_PATHDIALOG:
    case Q_STATE_UPLOAD_BATCH_DIALOG:
    case Q_STATE_UPLOAD:
    case Q_STATE_UPLOAD_BATCH:
    case Q_STATE_DOWNLOAD:
#endif
    case Q_STATE_EMULATION_MENU:
#ifndef Q_NO_KEYMACROS
    case Q_STATE_FUNCTION_KEY_EDITOR:
#endif
    case Q_STATE_CODEPAGE:
    case Q_STATE_INITIALIZATION:
    case Q_STATE_EXIT:
        /*
         * Don't flush.
         */
        break;
    }

}

/**
 * Draw screen for the Alt-Z console menu dialog.
 */
void console_menu_refresh() {
    char * status_string;
    int status_left_stop;
    int menu_left;
    int menu_top;
    const int screen_row = 1;
    const int settings_row = 6;
    const int other_row = 11;
    const int os_row = 16;
    const int toggles_row = 1;

    if (q_screen_dirty == Q_FALSE) {
        return;
    }

    /*
     * Clear screen for when it resizes
     */
    console_refresh(Q_FALSE);

    menu_left = (WIDTH - 60) / 2;
    menu_top = (HEIGHT - 21) / 2;

    screen_draw_box(menu_left, menu_top, menu_left + 60, menu_top + 21);

    screen_put_color_str_yx(menu_top, menu_left + 23, _(" COMMAND MENU "),
                            Q_COLOR_WINDOW_BORDER);
    screen_put_color_hline_yx(HEIGHT - 1, 0, cp437_chars[HATCH], WIDTH,
                              Q_COLOR_STATUS);

    status_string = _(" Select a Command    ESC/`-Return to TERMINAL Mode ");
    status_left_stop = WIDTH - strlen(status_string);
    if (status_left_stop <= 0) {
        status_left_stop = 0;
    } else {
        status_left_stop /= 2;
    }
    screen_put_color_str_yx(HEIGHT - 1, status_left_stop, status_string,
                            Q_COLOR_STATUS);

    /*
     * SCREEN
     */
    screen_put_color_hline_yx(menu_top + screen_row, menu_left + 2,
                              cp437_chars[SINGLE_BAR], 8, Q_COLOR_MENU_TEXT);
    screen_put_color_str_yx(menu_top + screen_row, menu_left + 10,
                            _(" SCREEN "), Q_COLOR_MENU_TEXT);
    screen_put_color_hline_yx(menu_top + screen_row, menu_left + 18,
                              cp437_chars[SINGLE_BAR], 9, Q_COLOR_MENU_TEXT);

    /*
     * C Clear screen
     */
    screen_put_color_str_yx(menu_top + screen_row + 1, menu_left + 2,
                            _("Alt-C  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Clear Screen"), Q_COLOR_MENU_TEXT);
    /*
     * S Split screen
     */
    screen_put_color_str_yx(menu_top + screen_row + 2, menu_left + 2,
                            _("Alt-S  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Split Screen"), Q_COLOR_MENU_TEXT);
    /*
     * T Screen dump
     */
    if (q_status.read_only == Q_TRUE) {
        screen_put_color_str_yx(menu_top + screen_row + 3, menu_left + 2,
                                _("Alt-T  "), Q_COLOR_MENU_COMMAND_UNAVAILABLE);
    } else {
        screen_put_color_str_yx(menu_top + screen_row + 3, menu_left + 2,
                                _("Alt-T  "), Q_COLOR_MENU_COMMAND);
    }
    screen_put_color_str(_("Screen Dump"), Q_COLOR_MENU_TEXT);
    /*
     * / Scrollback
     */
    screen_put_color_str_yx(menu_top + screen_row + 4, menu_left + 2,
                            _("Alt-/  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Scroll Back"), Q_COLOR_MENU_TEXT);

    /*
     * SETTINGS
     */
    screen_put_color_hline_yx(menu_top + settings_row, menu_left + 2,
                              cp437_chars[SINGLE_BAR], 8, Q_COLOR_MENU_TEXT);
    screen_put_color_str_yx(menu_top + settings_row, menu_left + 10,
                            _(" SETTINGS "),
                            Q_COLOR_MENU_TEXT);
    screen_put_color_hline_yx(menu_top + settings_row, menu_left + 20,
                              cp437_chars[SINGLE_BAR], 7, Q_COLOR_MENU_TEXT);
    /*
     * G Term emulation
     */
    screen_put_color_str_yx(menu_top + settings_row + 1, menu_left + 2,
                            _("Alt-G  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Term Emulation"), Q_COLOR_MENU_TEXT);
#ifndef Q_NO_KEYMACROS
    /*
     * J Function keys
     */
    screen_put_color_str_yx(menu_top + settings_row + 2, menu_left + 2,
                            _("Alt-J  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Function Keys"), Q_COLOR_MENU_TEXT);
#endif
    /*
     * N Configuration
     */
    screen_put_color_str_yx(menu_top + settings_row + 3, menu_left + 2,
                            _("Alt-N  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Configuration"), Q_COLOR_MENU_TEXT);
    /*
     * ; Codepage
     */
    screen_put_color_str_yx(menu_top + settings_row + 4, menu_left + 2,
                            _("Alt-;  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Codepage"), Q_COLOR_MENU_TEXT);

    /*
     * OS
     */
    screen_put_color_hline_yx(menu_top + os_row, menu_left + 2,
                              cp437_chars[SINGLE_BAR], 8, Q_COLOR_MENU_TEXT);
    screen_put_color_str_yx(menu_top + os_row, menu_left + 10, _(" OS "),
                            Q_COLOR_MENU_TEXT);
    screen_put_color_hline_yx(menu_top + os_row, menu_left + 14,
                              cp437_chars[SINGLE_BAR], 12, Q_COLOR_MENU_TEXT);
    /*
     * R OS shell
     */
    screen_put_color_str_yx(menu_top + os_row + 1, menu_left + 2, _("Alt-R  "),
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("OS Shell"), Q_COLOR_MENU_TEXT);
    /*
     * V View file
     */
    screen_put_color_str_yx(menu_top + os_row + 2, menu_left + 2, _("Alt-V  "),
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("View a File"), Q_COLOR_MENU_TEXT);
    /*
     * W List directory
     */
    screen_put_color_str_yx(menu_top + os_row + 3, menu_left + 2, _("Alt-W  "),
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("List Directory"), Q_COLOR_MENU_TEXT);

    /*
     * OTHER
     */
    screen_put_color_hline_yx(menu_top + other_row, menu_left + 2,
                              cp437_chars[SINGLE_BAR], 8, Q_COLOR_MENU_TEXT);
    screen_put_color_str_yx(menu_top + other_row, menu_left + 10, _(" OTHER "),
                            Q_COLOR_MENU_TEXT);
    screen_put_color_hline_yx(menu_top + other_row, menu_left + 17,
                              cp437_chars[SINGLE_BAR], 10, Q_COLOR_MENU_TEXT);
    /*
     * H Kill shell
     */
    screen_put_color_str_yx(menu_top + other_row + 1, menu_left + 2,
                            _("Alt-H  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Kill/Close Shell"), Q_COLOR_MENU_TEXT);
    /*
     * \ Alt Code Key
     */
    screen_put_color_str_yx(menu_top + other_row + 2, menu_left + 2,
                            _("Alt-\\  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Alt Code Key"), Q_COLOR_MENU_TEXT);
#ifndef Q_NO_PROTOCOLS
    /*
     * PgUp Upload files
     */
    screen_put_color_str_yx(menu_top + other_row + 3, menu_left + 2,
                            _(" PgUp  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Upload Files"), Q_COLOR_MENU_TEXT);
    /*
     * PgDn Download files
     */
    if (q_status.read_only == Q_TRUE) {
        screen_put_color_str_yx(menu_top + other_row + 4, menu_left + 2,
                                _(" PgDn  "), Q_COLOR_MENU_COMMAND_UNAVAILABLE);
    } else {
        screen_put_color_str_yx(menu_top + other_row + 4, menu_left + 2,
                                _(" PgDn  "), Q_COLOR_MENU_COMMAND);
    }
    screen_put_color_str(_("Download Files"), Q_COLOR_MENU_TEXT);
#endif

    /*
     * TOGGLES
     */
    screen_put_color_hline_yx(menu_top + toggles_row, menu_left + 32,
                              cp437_chars[SINGLE_BAR], 8, Q_COLOR_MENU_TEXT);
    screen_put_color_str_yx(menu_top + toggles_row, menu_left + 40,
                            _(" TOGGLES "), Q_COLOR_MENU_TEXT);
    screen_put_color_hline_yx(menu_top + toggles_row, menu_left + 49,
                              cp437_chars[SINGLE_BAR], 9, Q_COLOR_MENU_TEXT);
    /*
     * 7 Status line info
     */
    screen_put_color_str_yx(menu_top + toggles_row + 1, menu_left + 32,
                            _("Alt-7  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Status Line Info"), Q_COLOR_MENU_TEXT);
    /*
     * 8 Hi-bit strip
     */
    screen_put_color_str_yx(menu_top + toggles_row + 2, menu_left + 32,
                            _("Alt-8  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Hi-Bit Strip"), Q_COLOR_MENU_TEXT);

    /*
     * B Beeps and bells
     */
    screen_put_color_str_yx(menu_top + toggles_row + 3, menu_left + 32,
                            _("Alt-B  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Beeps & Bells"), Q_COLOR_MENU_TEXT);
    /*
     * E Half/full duplex
     */
    screen_put_color_str_yx(menu_top + toggles_row + 4, menu_left + 32,
                            _("Alt-E  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Half/Full Duplex"), Q_COLOR_MENU_TEXT);
    /*
     * P Capture file
     */
    if (q_status.read_only == Q_TRUE) {
        screen_put_color_str_yx(menu_top + toggles_row + 5, menu_left + 32,
                                _("Alt-P  "), Q_COLOR_MENU_COMMAND_UNAVAILABLE);
    } else {
        screen_put_color_str_yx(menu_top + toggles_row + 5, menu_left + 32,
                                _("Alt-P  "), Q_COLOR_MENU_COMMAND);
    }
    screen_put_color_str(_("Capture File"), Q_COLOR_MENU_TEXT);
    /*
     * U Scrollback record
     */
    screen_put_color_str_yx(menu_top + toggles_row + 6, menu_left + 32,
                            _("Alt-U  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Scrollback Record"), Q_COLOR_MENU_TEXT);
    /*
     * = Doorway mode
     */
    screen_put_color_str_yx(menu_top + toggles_row + 7, menu_left + 32,
                            _("Alt-=  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Doorway Mode"), Q_COLOR_MENU_TEXT);
    /*
     * - Status lines
     */
    screen_put_color_str_yx(menu_top + toggles_row + 8, menu_left + 32,
                            _("Alt--  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("Status Lines"), Q_COLOR_MENU_TEXT);
    /*
     * + CR/CRLF mode
     */
    screen_put_color_str_yx(menu_top + toggles_row + 9, menu_left + 32,
                            _("Alt-+  "), Q_COLOR_MENU_COMMAND);
    screen_put_color_str(_("CR/CRLF Mode"), Q_COLOR_MENU_TEXT);

    /*
     * Build date
     */
    screen_put_color_str_yx(menu_top + toggles_row + 16, menu_left + 32,
                            "LCXterm " Q_VERSION " " Q_VERSION_BRANCH,
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_printf_yx(menu_top + toggles_row + 17, menu_left + 32,
                               Q_COLOR_MENU_COMMAND, _("Compiled %s"),
                               __DATE__);

    screen_flush();
    q_screen_dirty = Q_FALSE;
}

/**
 * Keyboard handler for the Alt-Z console menu dialog.
 *
 * @param keystroke the keystroke from the user.
 * @param flags KEY_FLAG_ALT, KEY_FLAG_CTRL, etc.  See input.h.
 */
void console_menu_keyboard_handler(const int keystroke, const int flags) {

    DLOG(("console_menu_keyboard_handler() keystroke = %c flags = %d\n",
            (keystroke & 0xFF), flags));

    Q_DOORWAY_MODE old_doorway_mode = q_status.doorway_mode;

    /*
     * Process valid keystrokes through the console handler
     */
    if (flags & KEY_FLAG_ALT) {

        switch (keystroke) {

        case '7':
        case '8':
        case '-':
        case '+':
        case '=':
        case 'b':
        case 'c':
        case 'g':
        case 'h':
#ifndef Q_NO_KEYMACROS
        case 'j':
#endif
        case 'n':
        case 'p':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'B':
        case 'C':
        case 'G':
        case 'H':
#ifndef Q_NO_KEYMACROS
        case 'J':
#endif
        case 'N':
        case 'P':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case '/':
        case '\\':
        case ';':
            switch_state(Q_STATE_CONSOLE);
            q_screen_dirty = Q_TRUE;
            console_refresh(Q_TRUE);

            DLOG(("console_menu_keyboard_handler() keystroke = %c\n",
                    (keystroke & 0xFF)));

            /*
             * Permit keys from the menu to work in the console, even if
             * doorway is enabled.
             */
            if (keystroke == '=') {
                console_keyboard_handler(keystroke, flags);
            } else {
                DLOG(("console_menu_keyboard_handler() temporarily turn off doorway\n"));
                q_status.doorway_mode = Q_DOORWAY_MODE_OFF;
                console_keyboard_handler(keystroke, flags);
                q_status.doorway_mode = old_doorway_mode;
            }
            return;
        default:
            break;

        }
    }

    /*
     * Process valid keystrokes through the console handler
     */
    switch (keystroke) {

    case Q_KEY_PPAGE:
    case Q_KEY_NPAGE:
        switch_state(Q_STATE_CONSOLE);
        q_screen_dirty = Q_TRUE;
        console_refresh(Q_TRUE);
        console_keyboard_handler(keystroke, flags);
        return;

    default:
        break;
    }

    /*
     * Menu screen keystrokes
     */
    switch (keystroke) {

    case '`':
        /*
         * Backtick works too
         */
    case Q_KEY_ESCAPE:
        /*
         * ESC return to TERMINAL mode
         */
        switch_state(Q_STATE_CONSOLE);
        break;
    default:
        /*
         * Ignore keystroke
         */
        break;
    }
}
